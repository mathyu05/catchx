SELECT REPLACE(isc.COLUMN_NAME, ' ', '_') colName,
    CASE
        WHEN isc.DATA_TYPE = 'char' THEN 'string'
        WHEN isc.DATA_TYPE = 'datetime' THEN 'DateTime'
        WHEN isc.DATA_TYPE = 'varchar' THEN 'string'
        WHEN isc.DATA_TYPE = 'text' THEN 'string'
        WHEN isc.DATA_TYPE = 'smallint' AND isc.COLUMN_TYPE LIKE '%unsigned' THEN 'ushort'
        WHEN isc.DATA_TYPE = 'smallint' AND isc.COLUMN_TYPE NOT LIKE '%unsigned' THEN 'short'
        WHEN isc.DATA_TYPE = 'int' AND isc.COLUMN_TYPE LIKE '%unsigned' THEN 'uint'
        WHEN isc.DATA_TYPE = 'int' AND isc.COLUMN_TYPE NOT LIKE '%unsigned' THEN 'int'
        WHEN isc.DATA_TYPE = 'bigint' AND isc.COLUMN_TYPE LIKE '%unsigned' THEN 'ulong'
        WHEN isc.DATA_TYPE = 'bigint' AND isc.COLUMN_TYPE NOT LIKE '%unsigned' THEN 'long'
        WHEN isc.DATA_TYPE = 'tinyint' AND isc.COLUMN_TYPE LIKE '%unsigned' THEN 'ubyte'
        WHEN isc.DATA_TYPE = 'tinyint' AND isc.COLUMN_TYPE NOT LIKE '%unsigned' THEN 'byte'
    END coltype,
    CASE
        WHEN isc.IS_NULLABLE = 'YES' AND isc.DATA_TYPE IN ('datetime', 'smallint', 'int', 'bigint', 'tinyint')
        THEN '?'
        ELSE ''
    END nullabl
FROM information_schema.COLUMNS isc
WHERE isc.TABLE_SCHEMA = 'catchx'
    AND isc.TABLE_NAME = 'game'

SELECT isc.*
FROM information_schema.COLUMNS isc
WHERE isc.TABLE_SCHEMA = 'catchx'
    AND isc.TABLE_NAME = 'game'

SELECT 'e.' + isc.COLUMN_NAME + ',' colName,
    isc.column_name,
    isc.COLUMN_KEY
FROM information_schema.COLUMNS isc
WHERE isc.TABLE_SCHEMA = 'catchX'
    AND isc.TABLE_NAME = 'game'
    AND isc.COLUMN_KEY = 'PRI'
ORDER BY isc.ORDINAL_POSITION", tableName, schemaName);





    SELECT 
        REPLACE(iscc.COLUMN_NAME, ' ', '_') ColumnName,
        iscc.DATA_TYPE SQLColumnType,
        -- CASE
        --     WHEN col.max_length = -1 THEN
        --         case typ.name
        --             when 'char' then '(MAX)'
        --             when 'nchar' then '(MAX)'
        --             when 'text' then '(MAX)'
        --             when 'ntext' then '(MAX)'
        --             when 'varchar' then '(MAX)'
        --             when 'nvarchar' then '(MAX)'
        --             when 'datetime2' then '(' + CONVERT(NVARCHAR(10), col.scale) + ')'
        --             when 'decimal' then '(' + CONVERT(NVARCHAR(10), col.precision) + ',' + CONVERT(NVARCHAR, col.scale) + ')'
        --             when 'float' then 'SOMETHING'
        --             when 'numeric' then '(' + CONVERT(NVARCHAR(10), col.precision) + ',' + CONVERT(NVARCHAR, col.scale) + ')'
        --             else ''
        --         end
        --     WHEN col.max_length <> -1 THEN
        --         case typ.name
        --             when 'char' then '(' + CONVERT(NVARCHAR(10), isc.CHARACTER_MAXIMUM_LENGTH) + ')'
        --             when 'nchar' then '(' + CONVERT(NVARCHAR(10), isc.CHARACTER_MAXIMUM_LENGTH) + ')'
        --             when 'text' then '(' + CONVERT(NVARCHAR(10), isc.CHARACTER_MAXIMUM_LENGTH) + ')'
        --             when 'ntext' then '(' + CONVERT(NVARCHAR(10), isc.CHARACTER_MAXIMUM_LENGTH) + ')'
        --             when 'varchar' then '(' + CONVERT(NVARCHAR(10), isc.CHARACTER_MAXIMUM_LENGTH) + ')'
        --             when 'nvarchar' then '(' + CONVERT(NVARCHAR(10), isc.CHARACTER_MAXIMUM_LENGTH) + ')'
        --             when 'datetime2' then '(' + CONVERT(NVARCHAR(10), col.scale) + ')'
        --             when 'decimal' then '(' + CONVERT(NVARCHAR(10), col.precision) + ',' + CONVERT(NVARCHAR, col.scale) + ')'
        --             when 'float' then 'SOMETHING'
        --             when 'numeric' then '(' + CONVERT(NVARCHAR(10), col.precision) + ',' + CONVERT(NVARCHAR, col.scale) + ')'
        --             else ''
        --         end
        -- END {2},
        -- CASE
        --     WHEN iscc.CHARACTER_MAXIMUM_LENGTH IS NULL THEN
        --     case iscc.DATA_TYPE
        --         when 'char' then '
        --         .HasMaxLength(' + CONVERT(NVARCHAR(10), isc.CHARACTER_MAXIMUM_LENGTH) + ')'
        --         when 'nchar' then '
        --         .HasMaxLength(' + CONVERT(NVARCHAR(10), isc.CHARACTER_MAXIMUM_LENGTH) + ')'
        --         when 'text' then '
        --         .HasMaxLength(' + CONVERT(NVARCHAR(10), isc.CHARACTER_MAXIMUM_LENGTH) + ')'
        --         when 'ntext' then '
        --         .HasMaxLength(' + CONVERT(NVARCHAR(10), isc.CHARACTER_MAXIMUM_LENGTH) + ')'
        --         when 'varchar' then '
        --         .HasMaxLength(' + CONVERT(NVARCHAR(10), isc.CHARACTER_MAXIMUM_LENGTH) + ')'
        --         when 'nvarchar' then '
        --         .HasMaxLength(' + CONVERT(NVARCHAR(10), isc.CHARACTER_MAXIMUM_LENGTH) + ')'
        --             else ''
        --     END
        --     WHEN col.max_length = -1 THEN ''
        -- END {3},
        CASE
            WHEN iscc.EXTRA LIKE '%auto_increment%' THEN '
                .ValueGeneratedOnAdd()'
            ELSE ''
        END generation,
        -- case typ.name 
        --     when 'bigint' then 'long'
        --     when 'binary' then 'byte[]'
        --     when 'bit' then 'bool'
        --     when 'char' then 'string'
        --     when 'date' then 'DateTime'
        --     when 'datetime' then 'DateTime'
        --     when 'datetime2' then 'DateTime'
        --     when 'datetimeoffset' then 'DateTimeOffset'
        --     when 'decimal' then 'decimal'
        --     when 'float' then 'double'
        --     when 'image' then 'byte[]'
        --     when 'int' then 'int'
        --     when 'money' then 'decimal'
        --     when 'nchar' then 'string'
        --     when 'ntext' then 'string'
        --     when 'numeric' then 'decimal'
        --     when 'nvarchar' then 'string'
        --     when 'real' then 'float'
        --     when 'smalldatetime' then 'DateTime'
        --     when 'smallint' then 'short'
        --     when 'smallmoney' then 'decimal'
        --     when 'text' then 'string'
        --     when 'time' then 'TimeSpan'
        --     when 'timestamp' then 'long'
        --     when 'tinyint' then 'byte'
        --     when 'uniqueidentifier' then 'Guid'
        --     when 'varbinary' then 'byte[]'
        --     when 'varchar' then 'string'
        --     else 'UNKNOWN_' + typ.name
        -- end {6},
        case 
            when iscc.IS_NULLABLE = 'YES'
            then 'false' 
            else '' 
        end NullableSign
        -- CASE
        --     WHEN (typ.name <> 'bigint' AND typ.name <> 'binary' AND typ.name <> 'bit' AND typ.name <> 'date' AND typ.name <> 'datetime' AND typ.name <> 'datetime2' AND typ.name <> 'datetimeoffset' AND typ.name <> 'decimal' AND typ.name <> 'float' AND typ.name <> 'image' AND typ.name <> 'int' AND typ.name <> 'money' AND typ.name <> 'numeric' AND typ.name <> 'real' AND typ.name <> 'smalldatetime' AND typ.name <> 'smallint' AND typ.name <> 'smallmoney' AND typ.name <> 'time' AND typ.name <> 'timestamp' AND typ.name <> 'tinyint' AND typ.name <> 'uniqueidentifier' AND typ.name <> 'varbinary') THEN '
        --         .HasConversion(input => string.IsNullOrEmpty(input) ? input : input.Trim(), output => string.IsNullOrEmpty(output) ? output : output.Trim()'
        --     ELSE ''
        -- END {10},
        -- CASE
        --     WHEN oaisc.COLUMN_KEY = 'PRI' AND isccuistc.TABLE_NAME = '{9}' AND (typ.name <> 'bigint' AND typ.name <> 'binary' AND typ.name <> 'bit' AND typ.name <> 'date' AND typ.name <> 'datetime' AND typ.name <> 'datetime2' AND typ.name <> 'datetimeoffset' AND typ.name <> 'decimal' AND typ.name <> 'float' AND typ.name <> 'image' AND typ.name <> 'int' AND typ.name <> 'money' AND typ.name <> 'numeric' AND typ.name <> 'real' AND typ.name <> 'smalldatetime' AND typ.name <> 'smallint' AND typ.name <> 'smallmoney' AND typ.name <> 'time' AND typ.name <> 'timestamp' AND typ.name <> 'tinyint' AND typ.name <> 'uniqueidentifier' AND typ.name <> 'varbinary') THEN
        --         '.ToUpperInvariant())'
        --     WHEN (typ.name <> 'bigint' AND typ.name <> 'binary' AND typ.name <> 'bit' AND typ.name <> 'date' AND typ.name <> 'datetime' AND typ.name <> 'datetime2' AND typ.name <> 'datetimeoffset' AND typ.name <> 'decimal' AND typ.name <> 'float' AND typ.name <> 'image' AND typ.name <> 'int' AND typ.name <> 'money' AND typ.name <> 'numeric' AND typ.name <> 'real' AND typ.name <> 'smalldatetime' AND typ.name <> 'smallint' AND typ.name <> 'smallmoney' AND typ.name <> 'time' AND typ.name <> 'timestamp' AND typ.name <> 'tinyint' AND typ.name <> 'uniqueidentifier' AND typ.name <> 'varbinary') THEN
        --         ')'
        --     ELSE ''
        -- END {11}
    from information_schema.COLUMNS iscc
        APPLY
        (
            SELECT isc.COLUMN_KEY
            FROM information_schema.COLUMNS isc
            WHERE isc.COLUMN_NAME = 'id'
                AND isc.TABLE_NAME = 'game'
                AND isc.TABLE_SCHEMA = 'catchx'
            ORDER BY
                CASE
                    WHEN isc.COLUMN_KEY = 'PRI' THEN NULL
                    ELSE isc.COLUMN_KEY
                END
                ASC
        ) oaisc
    where iscc.TABLE_SCHEMA = 'catchx'
        AND iscc.TABLE_NAME = 'game'
    ORDER BY iscc.ORDINAL_POSITION", ColumnName, SQLColumnType, LengthAdorn, maxyLength, generation, ColumnId, ColumnType, NullableSign, schemaName, tableName, conversion, conversionUpper);




select Id, Name from catchx.game order by Id

, columnId, columnName, tableName, schemaName)

SELECT *
-- DELETE
FROM CatchX.TransportMode

INSERT INTO TransportMode(Name)
VALUES ('Taxi'),
    ('Bus'),
    ('Train'),
    ('Ferry')

SELECT *
FROM CatchX.MapVertexLocationTypeEnum