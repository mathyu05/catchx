using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace CatchXApi.Extensions
{
    public static class DbSetExtensions
    {
        public static void SeedEnumValues<T, TEnum>(this DbSet<T> dbSet, Func<TEnum, T> converter) where T : class
        {
            List<T> values = Enum.GetValues(typeof(TEnum))
                .Cast<object>()
                .Select(value => converter((TEnum)value))
                .ToList();

            foreach (T value in values)
            {
                if (dbSet.Contains(value))
                {
                    dbSet.Update(value);
                }
                else
                {
                    dbSet.Add(value);
                }
            }
        }
    }
}