using System.Collections.Generic;
using AutoMapper;
using CatchXApi.Data.Dtos;
using CatchXApi.Exceptions;
using CatchXApi.Data.Models;
using CatchXApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CatchXApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class MapController : ControllerBase
    {
        private IMapService _mapService;
        private IMapper _mapper;

        public MapController(IMapService mapService, IMapper mapper)
        {
            _mapService = mapService;
            _mapper = mapper;
        }

        [HttpPost("create")]
        public IActionResult Create([FromBody] MapDto mapDto)
        {
            Map map = _mapper.Map<Map>(mapDto);

            try
            {
                _mapService.Create(map);
                return Ok();
            }
            catch(AppException appException)
            {
                return BadRequest(
                    new
                    {
                        message = appException.Message
                    }
                );
            }
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            IEnumerable<Map> maps = _mapService.GetAll();
            IList<MapDto> mapDtos = _mapper.Map<IList<MapDto>>(maps);

            return Ok(mapDtos);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(uint id)
        {
            Map map = _mapService.GetById(id);
            MapDto mapDto = _mapper.Map<MapDto>(map);

            return Ok(mapDto);
        }

        [HttpPost("add/transportmode")]
        public IActionResult AddTransportMode([FromBody] MapTransportModeDto mapTransportModeDto)
        {
            MapTransportMode mapTransportMode = _mapper.Map<MapTransportMode>(mapTransportModeDto);

            try
            {
                _mapService.AddTransportMode(mapTransportMode);
                return Ok();
            }
            catch(AppException appException)
            {
                return BadRequest(
                    new
                    {
                        message = appException.Message
                    }
                );
            }
        }
        
        [HttpPost("add/location")]
        public IActionResult AddLocation([FromBody] MapLocationDto mapLocationDto)
        {
            MapLocation mapLocation = _mapper.Map<MapLocation>(mapLocationDto);

            try
            {
                _mapService.AddLocation(mapLocation);
                return Ok();
            }
            catch(AppException appException)
            {
                return BadRequest(
                    new
                    {
                        message = appException.Message
                    }
                );
            }
        }
        
        [HttpPost("add/path")]
        public IActionResult AddPath([FromBody] MapPathDto mapPathDto)
        {
            MapPath mapPath = _mapper.Map<MapPath>(mapPathDto);

            try
            {
                _mapService.AddPath(mapPath, mapPathDto.LocationOneId, mapPathDto.LocationTwoId);
                return Ok();
            }
            catch(AppException appException)
            {
                return BadRequest(
                    new
                    {
                        message = appException.Message
                    }
                );
            }
        }
    }
}