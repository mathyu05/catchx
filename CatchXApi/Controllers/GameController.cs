using AutoMapper;
using CatchXApi.Data.Dtos;
using CatchXApi.Data.Models;
using CatchXApi.Exceptions;
using CatchXApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CatchXApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class GameController : ControllerBase
    {
        private IGameService _gameService;
        private IMapper _mapper;

        public GameController(IGameService gameService, IMapper mapper)
        {
            _gameService = gameService;
            _mapper = mapper;
        }

        [HttpPost("create")]
        public IActionResult Create([FromBody] GameDto gameDto)
        {
            Game game = _mapper.Map<Game>(gameDto);

            try
            {
                _gameService.Create(game);
                return Ok();
            }
            catch(AppException appException)
            {
                return BadRequest(
                    new
                    {
                        message = appException.Message
                    }
                );
            }
        }

        [HttpPost("add/specialtyToken")]
        public IActionResult AddSpecialtyToken([FromBody] GameSpecialtyTokenDto gameSpecialtyTokenDto)
        {
            GameSpecialtyToken gameSpecialtyToken = _mapper.Map<GameSpecialtyToken>(gameSpecialtyTokenDto);

            try
            {
                _gameService.AddSpecialtyToken(gameSpecialtyToken);
                return Ok();
            }
            catch(AppException appException)
            {
                return BadRequest(
                    new
                    {
                        message = appException.Message
                    }
                );
            }
        }

        [HttpGet("openlobby")]
        public IActionResult OpenLobby()
        {
            try
            {
                _gameService.OpenLobby();
            }
            catch(AppException appException)
            {
                return BadRequest(
                    new
                    {
                        message = appException.Message
                    }
                );
            }
        }

        [HttpGet("join")]
        public IActionResult Join([FromQuery] uint gameId, uint playerId)
        {
            try
            {
                _gameService.Join(gameId, playerId);
                return Ok();
            }
            catch (AppException appException)
            {
                return BadRequest(
                    new
                    {
                        message = appException.Message
                    }
                );
            }
        }

        [HttpGet("leave")]
        public IActionResult Leave([FromQuery] uint gameId, uint playerId)
        {
            try
            {
                _gameService.Leave(gameId, playerId);
                return Ok();
            }
            catch (AppException appException)
            {
                return BadRequest(
                    new
                    {
                        message = appException.Message
                    }
                );
            }
        }

        [HttpGet("start")]
        public IActionResult Start([FromQuery] uint gameId)
        {
            try
            {
                _gameService.Start(gameId);
                return Ok();
            }
            catch (AppException appException)
            {
                return BadRequest(
                    new
                    {
                        message = appException.Message
                    }
                );
            }
        }
    }
}