using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using AutoMapper;
using CatchXApi.Data.Dtos;
using CatchXApi.Exceptions;
using CatchXApi.Helpers;
using CatchXApi.Data.Models;
using CatchXApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace CatchXApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class PlayerController : ControllerBase
    {
        private IPlayerService _playerService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public PlayerController(IPlayerService playerService,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _playerService = playerService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody] PlayerDto playerDto)
        {
            Player player = _playerService.Authenticate(playerDto.Name, playerDto.Password);

            if (player == null)
            {
                return BadRequest(new { message = "Name or password is incorrect" });
            }

            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            byte[] key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(
                    new Claim[]
                    {
                        new Claim(ClaimTypes.Name, player.Id.ToString())
                    }
                ),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);
            string tokenString = tokenHandler.WriteToken(token);

            return Ok(
                new
                {
                    Id = player.Id,
                    Name = player.Name,
                    Token = tokenString
                }
            );
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public IActionResult Register([FromBody] PlayerDto playerDto)
        {
            Player player = _mapper.Map<Player>(playerDto);

            try
            {
                _playerService.Create(player, playerDto.Password);
                return Ok();
            }
            catch(AppException appException)
            {
                return BadRequest(
                    new
                    {
                        message = appException.Message
                    }
                );
            }
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            IEnumerable<Player> players = _playerService.GetAll();
            IList<PlayerDto> playerDtos = _mapper.Map<IList<PlayerDto>>(players);

            return Ok(playerDtos);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(uint id)
        {
            Player player = _playerService.GetById(id);
            PlayerDto playerDto = _mapper.Map<PlayerDto>(player);

            return Ok(playerDto);
        }

        [HttpPut("{id}")]
        public IActionResult Update(uint id, [FromBody] PlayerDto playerDto)
        {
            Player player = _mapper.Map<Player>(playerDto);
            player.Id = id;

            try
            {
                _playerService.Update(player, playerDto.Password);

                return Ok();
            }
            catch (AppException appException)
            {
                return BadRequest(
                    new
                    {
                        message = appException.Message
                    }
                );
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(uint id)
        {
            // TODO Do delete?

            return Ok();
        }
    }
}