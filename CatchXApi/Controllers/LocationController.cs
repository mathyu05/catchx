using System.Collections.Generic;
using AutoMapper;
using CatchXApi.Data.Dtos;
using CatchXApi.Exceptions;
using CatchXApi.Data.Models;
using CatchXApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CatchXApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class LocationController : ControllerBase
    {
        private ILocationService _locationService;
        private IMapper _mapper;

        public LocationController(ILocationService locationService,
            IMapper mapper)
        {
            _locationService = locationService;
            _mapper = mapper;
        }

        [HttpPost("create")]
        public IActionResult Create([FromBody] LocationDto locationDto)
        {
            Location location = _mapper.Map<Location>(locationDto);

            try
            {
                _locationService.Create(location);
                return Ok();
            }
            catch(AppException appException)
            {
                return BadRequest(
                    new
                    {
                        message = appException.Message
                    }
                );
            }
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            IEnumerable<Location> locations = _locationService.GetAll();
            IList<LocationDto> locationDtos = _mapper.Map<IList<LocationDto>>(locations);

            return Ok(locationDtos);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(uint id)
        {
            Location location = _locationService.GetById(id);
            LocationDto locationDto = _mapper.Map<LocationDto>(location);

            return Ok(locationDto);
        }

        [HttpPut("{id}")]
        public IActionResult Update(uint id, [FromBody] LocationDto locationDto)
        {
            Location location = _mapper.Map<Location>(locationDto);
            location.Id = id;

            try
            {
                _locationService.Update(location);

                return Ok();
            }
            catch (AppException appException)
            {
                return BadRequest(
                    new
                    {
                        message = appException.Message
                    }
                );
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(uint id)
        {
            // TODO Do delete?

            return Ok();
        }
    }
}