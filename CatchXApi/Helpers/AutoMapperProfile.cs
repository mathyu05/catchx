using AutoMapper;
using CatchXApi.Data.Dtos;
using CatchXApi.Data.Models;

namespace CatchXApi.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Player, PlayerDto>();
            CreateMap<PlayerDto, Player>();

            CreateMap<Location, LocationDto>();
            CreateMap<LocationDto, Location>();

            CreateMap<Map, MapDto>();
            CreateMap<MapDto, Map>();

            CreateMap<MapTransportMode, MapTransportModeDto>();
            CreateMap<MapTransportModeDto, MapTransportMode>();

            CreateMap<MapLocation, MapLocationDto>();
            CreateMap<MapLocationDto, MapLocation>();

            CreateMap<MapPath, MapPathDto>();
            CreateMap<MapPathDto, MapPath>();

            CreateMap<Game, GameDto>();
            CreateMap<GameDto, Game>();

            CreateMap<GameSpecialtyToken, GameSpecialtyTokenDto>();
            CreateMap<GameSpecialtyTokenDto, GameSpecialtyToken>();
        }
    }
}