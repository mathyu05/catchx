﻿using System;
using System.Text;
using System.Threading.Tasks;
using CatchXApi.Helpers;
using CatchXApi.Services;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Http;
using CatchXApi.Data;
using CatchXApi.Data.Models;

namespace CatchXApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();

            string connectionString = Configuration.GetConnectionString("CatchXDatabase");

            services.AddDbContext<CatchXContext>(
                options => options.UseMySql(
                    connectionString,
                    mySqlOptions =>
                    {
                        mySqlOptions.ServerVersion(
                            new Version(10, 3, 15),
                            Pomelo.EntityFrameworkCore.MySql.Infrastructure.ServerType.MySql
                        );
                    }
                )
            );
            
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            MapperConfiguration mapperConfig = new MapperConfiguration(
                mc =>
                {
                    mc.AddProfile(new AutoMapperProfile());
                }
            );

            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);

            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            AppSettings appSettings = appSettingsSection.Get<AppSettings>();
            byte[] key = Encoding.ASCII.GetBytes(appSettings.Secret);

            services.AddAuthentication(
                a =>
                {
                    a.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    a.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(
                    a => 
                    {
                        a.Events = new JwtBearerEvents
                        {
                            OnTokenValidated = context =>
                            {
                                var playerService = context.HttpContext.RequestServices.GetRequiredService<IPlayerService>();
                                uint playerId = uint.Parse(context.Principal.Identity.Name);
                                Player player = playerService.GetById(playerId);

                                if (player == null)
                                {
                                    context.Fail("Unauthorized");
                                }

                                return Task.CompletedTask;
                            }
                        };
                        a.RequireHttpsMetadata = false;
                        a.SaveToken = true;
                        a.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuerSigningKey = true,
                            IssuerSigningKey = new SymmetricSecurityKey(key),
                            ValidateIssuer = false,
                            ValidateAudience = false
                        };
                    }
                );

                services.AddScoped<IPlayerService, PlayerService>();
                services.AddScoped<ILocationService, LocationService>();
                services.AddScoped<IMapService, MapService>();
                services.AddScoped<IGameService, GameService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                // app.UseHsts();
            }

            app.UseCors(
                a => a.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
            );

            app.UseAuthentication();

            // app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
