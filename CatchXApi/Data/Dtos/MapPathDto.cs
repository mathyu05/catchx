namespace CatchXApi.Data.Dtos
{
    public class MapPathDto
    {
        public uint Id { get; set; }
        public uint MapId { get; set; }
        public ushort TransportModeId { get; set; }
        public uint LocationOneId { get; set; }
        public uint LocationTwoId { get; set; }
    }
}