namespace CatchXApi.Data.Dtos
{
    public class MapTransportModeDto
    {
        public uint MapId { get; set; }
        public ushort TransportModeId { get; set; }
        public byte DetectiveStartingTickets { get; set; }
        public byte FelonStartingTickets { get; set; }
    }
}