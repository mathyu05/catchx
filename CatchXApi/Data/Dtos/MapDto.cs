namespace CatchXApi.Data.Dtos
{
    public class MapDto
    {
        public uint Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}