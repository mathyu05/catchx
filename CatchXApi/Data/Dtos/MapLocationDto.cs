namespace CatchXApi.Data.Dtos
{
    public class MapLocationDto
    {
        public uint MapId { get; set; }
        public uint LocationId { get; set; }
    }
}