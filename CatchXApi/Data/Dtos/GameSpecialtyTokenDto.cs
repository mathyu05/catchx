namespace CatchXApi.Data.Dtos
{
    public class GameSpecialtyTokenDto
    {
        public uint GameId { get; set; }
        public byte SpecialtyTokenId { get; set; }
        public byte DetectiveStartingTokens { get; set; }
        public byte FelonStartingTokens { get; set; }
    }
}