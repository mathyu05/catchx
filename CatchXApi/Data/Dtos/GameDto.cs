namespace CatchXApi.Data.Dtos
{
    public class GameDto
    {
        public uint Id { get; set; }
        public string Name { get; set; }
        public uint MapId { get; set; }
        public byte NumberOfPlayers { get; set; }
    }
}