namespace CatchXApi.Data.Dtos
{
    public class PlayerDto
    {
        public uint Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        
        public string Password { get; set; }
    }
}