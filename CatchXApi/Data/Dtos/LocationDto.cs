namespace CatchXApi.Data.Dtos
{
    public class LocationDto
    {
        public uint Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitute { get; set; }
    }
}