﻿namespace CatchXApi.Data.Models
{
    public partial class MapTransportMode
    {
        public uint MapId { get; set; }
        public ushort TransportModeId { get; set; }
        public byte DetectiveStartingTickets { get; set; }
        public byte FelonStartingTickets { get; set; }

        public virtual Map Map { get; set; }
        public virtual TransportMode TransportMode { get; set; }
    }
}
