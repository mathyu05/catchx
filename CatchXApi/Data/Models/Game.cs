﻿using System;
using System.Collections.Generic;

namespace CatchXApi.Data.Models
{
    public partial class Game
    {
        public Game()
        {
            GamePlayer = new HashSet<GamePlayer>();
            GamePlayerMove = new HashSet<GamePlayerMove>();
            GameSpecialtyToken = new HashSet<GameSpecialtyToken>();
        }

        public uint Id { get; set; }
        public string Name { get; set; }
        public uint MapId { get; set; }
        public byte NumberOfPlayers { get; set; }
        public byte GameStatusId { get; set; }
        public uint? CreatorPlayerId { get; set; }
        public DateTime CreateDate { get; set; }

        public virtual Player CreatorPlayer { get; set; }
        public virtual GameStatus GameStatus { get; set; }
        public virtual Map Map { get; set; }
        public virtual ICollection<GamePlayer> GamePlayer { get; set; }
        public virtual ICollection<GamePlayerMove> GamePlayerMove { get; set; }
        public virtual ICollection<GameSpecialtyToken> GameSpecialtyToken { get; set; }
    }
}
