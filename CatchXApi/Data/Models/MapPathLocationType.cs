﻿using System.Collections.Generic;
using CatchXApi.Data.Enums;

namespace CatchXApi.Data.Models
{
    public partial class MapPathLocationType
    {
        public MapPathLocationType()
        {
            MapPathLocation = new HashSet<MapPathLocation>();
        }

        private MapPathLocationType(MapPathLocationTypeEnum @enum)
        {
            Id = (byte)@enum;
            Name = @enum.ToString();
            
            MapPathLocation = new HashSet<MapPathLocation>();
        }

        public static implicit operator MapPathLocationType(MapPathLocationTypeEnum @enum)
        {
            return new MapPathLocationType(@enum);
        }

        public static implicit operator MapPathLocationTypeEnum(MapPathLocationType mapPathLocationType)
        {
            return (MapPathLocationTypeEnum)mapPathLocationType.Id;
        }

        public byte Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<MapPathLocation> MapPathLocation { get; set; }
    }
}
