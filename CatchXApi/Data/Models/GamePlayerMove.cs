﻿using System.Collections.Generic;

namespace CatchXApi.Data.Models
{
    public partial class GamePlayerMove
    {
        public GamePlayerMove()
        {
            InversePlayerPreviousMove = new HashSet<GamePlayerMove>();
        }

        public ushort Id { get; set; }
        public uint GameId { get; set; }
        public uint PlayerId { get; set; }
        public uint LocationId { get; set; }
        public ushort TransportModeId { get; set; }
        public ushort? PlayerPreviousMoveId { get; set; }

        public virtual Game Game { get; set; }
        public virtual Location Location { get; set; }
        public virtual Player Player { get; set; }
        public virtual GamePlayerMove PlayerPreviousMove { get; set; }
        public virtual TransportMode TransportMode { get; set; }
        public virtual ICollection<GamePlayerMove> InversePlayerPreviousMove { get; set; }
    }
}
