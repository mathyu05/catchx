﻿using System;
using System.Collections.Generic;

namespace CatchXApi.Data.Models
{
    public partial class Player
    {
        public Player()
        {
            Game = new HashSet<Game>();
            GamePlayer = new HashSet<GamePlayer>();
            GamePlayerMove = new HashSet<GamePlayerMove>();
            Map = new HashSet<Map>();
        }

        public uint Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public byte PlayerTypeId { get; set; }
        public byte PlayerGenderId { get; set; }
        public uint GamesCompleted { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? DeleteDate { get; set; }

        public virtual PlayerGender PlayerGender { get; set; }
        public virtual PlayerType PlayerType { get; set; }
        public virtual ICollection<Game> Game { get; set; }
        public virtual ICollection<GamePlayer> GamePlayer { get; set; }
        public virtual ICollection<GamePlayerMove> GamePlayerMove { get; set; }
        public virtual ICollection<Map> Map { get; set; }
    }
}
