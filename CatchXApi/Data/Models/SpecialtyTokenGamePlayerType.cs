﻿namespace CatchXApi.Data.Models
{
    public partial class SpecialtyTokenGamePlayerType
    {
        public byte SpecialtyTokenId { get; set; }
        public byte GamePlayerTypeId { get; set; }

        public virtual GamePlayerType GamePlayerType { get; set; }
        public virtual SpecialtyToken SpecialtyToken { get; set; }
    }
}
