﻿using System;
using System.Collections.Generic;

namespace CatchXApi.Data.Models
{
    public partial class Map
    {
        public Map()
        {
            Game = new HashSet<Game>();
            MapLocation = new HashSet<MapLocation>();
            MapTransportMode = new HashSet<MapTransportMode>();
            MapPath = new HashSet<MapPath>();
        }

        public uint Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public uint? CreatorPlayerId { get; set; }
        public DateTime CreateDate { get; set; }

        public virtual Player CreatorPlayer { get; set; }
        public virtual ICollection<Game> Game { get; set; }
        public virtual ICollection<MapLocation> MapLocation { get; set; }
        public virtual ICollection<MapTransportMode> MapTransportMode { get; set; }
        public virtual ICollection<MapPath> MapPath { get; set; }
    }
}
