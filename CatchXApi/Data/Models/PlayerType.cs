﻿using System.Collections.Generic;
using CatchXApi.Data.Enums;

namespace CatchXApi.Data.Models
{
    public partial class PlayerType
    {
        public PlayerType()
        {
            Player = new HashSet<Player>();
        }

        private PlayerType(PlayerTypeEnum @enum)
        {
            Id = (byte)@enum;
            Name = @enum.ToString();
            
            Player = new HashSet<Player>();
        }

        public static implicit operator PlayerType(PlayerTypeEnum @enum)
        {
            return new PlayerType(@enum);
        }

        public static implicit operator PlayerTypeEnum(PlayerType playerType)
        {
            return (PlayerTypeEnum)playerType.Id;
        }

        public byte Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Player> Player { get; set; }
    }
}
