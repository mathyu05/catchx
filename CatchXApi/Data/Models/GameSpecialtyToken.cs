﻿namespace CatchXApi.Data.Models
{
    public partial class GameSpecialtyToken
    {
        public uint GameId { get; set; }
        public byte SpecialtyTokenId { get; set; }
        public byte DetectiveStartingTokens { get; set; }
        public byte FelonStartingTokens { get; set; }

        public virtual Game Game { get; set; }
        public virtual SpecialtyToken SpecialtyToken { get; set; }
    }
}
