﻿using System.Collections.Generic;

namespace CatchXApi.Data.Models
{
    public partial class Location
    {
        public Location()
        {
            GamePlayer = new HashSet<GamePlayer>();
            GamePlayerMove = new HashSet<GamePlayerMove>();
            MapLocation = new HashSet<MapLocation>();
            MapPathLocation = new HashSet<MapPathLocation>();
        }

        public uint Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }

        public virtual ICollection<GamePlayer> GamePlayer { get; set; }
        public virtual ICollection<GamePlayerMove> GamePlayerMove { get; set; }
        public virtual ICollection<MapLocation> MapLocation { get; set; }
        public virtual ICollection<MapPathLocation> MapPathLocation { get; set; }
    }
}
