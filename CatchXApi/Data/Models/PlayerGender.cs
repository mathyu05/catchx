﻿using System.Collections.Generic;
using CatchXApi.Data.Enums;

namespace CatchXApi.Data.Models
{
    public partial class PlayerGender
    {
        public PlayerGender()
        {
            Player = new HashSet<Player>();
        }

        private PlayerGender(PlayerGenderEnum @enum)
        {
            Id = (byte)@enum;
            Name = @enum.ToString();
            
            Player = new HashSet<Player>();
        }

        public static implicit operator PlayerGender(PlayerGenderEnum @enum)
        {
            return new PlayerGender(@enum);
        }

        public static implicit operator PlayerGenderEnum(PlayerGender playerGender)
        {
            return (PlayerGenderEnum)playerGender.Id;
        }

        public byte Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Player> Player { get; set; }
    }
}
