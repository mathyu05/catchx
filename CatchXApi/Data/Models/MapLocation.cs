﻿namespace CatchXApi.Data.Models
{
    public partial class MapLocation
    {
        public uint MapId { get; set; }
        public uint LocationId { get; set; }

        public virtual Location Location { get; set; }
        public virtual Map Map { get; set; }
    }
}
