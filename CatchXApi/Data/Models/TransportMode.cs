﻿using System.Collections.Generic;

namespace CatchXApi.Data.Models
{
    public partial class TransportMode
    {
        public TransportMode()
        {
            GamePlayerMove = new HashSet<GamePlayerMove>();
            MapTransportMode = new HashSet<MapTransportMode>();
            MapPath = new HashSet<MapPath>();
        }

        public ushort Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<GamePlayerMove> GamePlayerMove { get; set; }
        public virtual ICollection<MapTransportMode> MapTransportMode { get; set; }
        public virtual ICollection<MapPath> MapPath { get; set; }
    }
}
