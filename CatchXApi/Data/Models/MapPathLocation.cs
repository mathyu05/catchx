﻿namespace CatchXApi.Data.Models
{
    public partial class MapPathLocation
    {
        public uint MapPathId { get; set; }
        public uint LocationId { get; set; }
        public byte MapPathLocationTypeId { get; set; }

        public virtual Location Location { get; set; }
        public virtual MapPath MapPath { get; set; }
        public virtual MapPathLocationType MapPathLocationType { get; set; }
    }
}
