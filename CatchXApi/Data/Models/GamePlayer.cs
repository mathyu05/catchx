﻿namespace CatchXApi.Data.Models
{
    public partial class GamePlayer
    {
        public uint GameId { get; set; }
        public uint PlayerId { get; set; }
        public byte GamePlayerTypeId { get; set; }
        public byte? OrderOfPlay { get; set; }
        public uint? StartingLocationId { get; set; }

        public virtual Game Game { get; set; }
        public virtual GamePlayerType GamePlayerType { get; set; }
        public virtual Player Player { get; set; }
        public virtual Location StartingLocation { get; set; }
    }
}
