﻿using System.Collections.Generic;

namespace CatchXApi.Data.Models
{
    public partial class MapPath
    {
        public MapPath()
        {
            MapPathLocation = new HashSet<MapPathLocation>();
        }

        public uint Id { get; set; }
        public uint MapId { get; set; }
        public ushort TransportModeId { get; set; }

        public virtual Map Map { get; set; }
        public virtual TransportMode TransportMode { get; set; }
        public virtual ICollection<MapPathLocation> MapPathLocation { get; set; }
    }
}
