﻿using System.Collections.Generic;
using CatchXApi.Data.Enums;

namespace CatchXApi.Data.Models
{
    public partial class SpecialtyToken
    {
        public SpecialtyToken()
        {
            GameSpecialtyToken = new HashSet<GameSpecialtyToken>();
            SpecialtyTokenGamePlayerType = new HashSet<SpecialtyTokenGamePlayerType>();
        }

        private SpecialtyToken(SpecialtyTokenEnum @enum)
        {
            Id = (byte)@enum;
            Name = @enum.ToString();
            
            GameSpecialtyToken = new HashSet<GameSpecialtyToken>();
            SpecialtyTokenGamePlayerType = new HashSet<SpecialtyTokenGamePlayerType>();
        }

        public static implicit operator SpecialtyToken(SpecialtyTokenEnum @enum)
        {
            return new SpecialtyToken(@enum);
        }

        public static implicit operator SpecialtyTokenEnum(SpecialtyToken specialtyToken)
        {
            return (SpecialtyTokenEnum)specialtyToken.Id;
        }

        public byte Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<GameSpecialtyToken> GameSpecialtyToken { get; set; }
        public virtual ICollection<SpecialtyTokenGamePlayerType> SpecialtyTokenGamePlayerType { get; set; }
    }
}
