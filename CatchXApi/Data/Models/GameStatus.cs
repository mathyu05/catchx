﻿using System.Collections.Generic;
using CatchXApi.Data.Enums;

namespace CatchXApi.Data.Models
{
    public partial class GameStatus
    {
        public GameStatus()
        {
            Game = new HashSet<Game>();
        }

        private GameStatus(GameStatusEnum @enum)
        {
            Id = (byte)@enum;
            Name = @enum.ToString();
            
            Game = new HashSet<Game>();
        }

        public static implicit operator GameStatus(GameStatusEnum @enum)
        {
            return new GameStatus(@enum);
        }

        public static implicit operator GameStatusEnum(GameStatus gameStatus)
        {
            return (GameStatusEnum)gameStatus.Id;
        }

        public byte Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Game> Game { get; set; }
    }
}
