﻿using System.Collections.Generic;
using CatchXApi.Data.Enums;

namespace CatchXApi.Data.Models
{
    public partial class GamePlayerType
    {
        public GamePlayerType()
        {
            GamePlayer = new HashSet<GamePlayer>();
            SpecialtyTokenGamePlayerType = new HashSet<SpecialtyTokenGamePlayerType>();
        }

        private GamePlayerType(GamePlayerTypeEnum @enum)
        {
            Id = (byte)@enum;
            Name = @enum.ToString();
            
            GamePlayer = new HashSet<GamePlayer>();
            SpecialtyTokenGamePlayerType = new HashSet<SpecialtyTokenGamePlayerType>();
        }

        public static implicit operator GamePlayerType(GamePlayerTypeEnum @enum)
        {
            return new GamePlayerType(@enum);
        }

        public static implicit operator GamePlayerTypeEnum(GamePlayerType gamePlayerType)
        {
            return (GamePlayerTypeEnum)gamePlayerType.Id;
        }

        public byte Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<GamePlayer> GamePlayer { get; set; }
        public virtual ICollection<SpecialtyTokenGamePlayerType> SpecialtyTokenGamePlayerType { get; set; }
    }
}
