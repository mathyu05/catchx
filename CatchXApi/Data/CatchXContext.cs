﻿using CatchXApi.Data.Enums;
using CatchXApi.Data.Maps;
using CatchXApi.Data.Models;
using CatchXApi.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace CatchXApi.Data
{
    public partial class CatchXContext : DbContext
    {
        public IConfiguration Configuration { get; }
        
        public CatchXContext(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public CatchXContext(IConfiguration configuration, DbContextOptions<CatchXContext> options)
            : base(options)
        {
            Configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseMySql(Configuration.GetConnectionString("CatchXDatabase"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            new GameMap(modelBuilder.Entity<Game>());
            new GamePlayerMap(modelBuilder.Entity<GamePlayer>());
            new GamePlayerMoveMap(modelBuilder.Entity<GamePlayerMove>());
            new GamePlayerTypeMap(modelBuilder.Entity<GamePlayerType>());
            new GameSpecialtyTokenMap(modelBuilder.Entity<GameSpecialtyToken>());
            new GameStatusMap(modelBuilder.Entity<GameStatus>());
            new LocationMap(modelBuilder.Entity<Location>());
            new MapMap(modelBuilder.Entity<Map>());
            new MapLocationMap(modelBuilder.Entity<MapLocation>());
            new MapTransportModeMap(modelBuilder.Entity<MapTransportMode>());
            new MapPathMap(modelBuilder.Entity<MapPath>());
            new MapPathLocationMap(modelBuilder.Entity<MapPathLocation>());
            new MapPathLocationTypeMap(modelBuilder.Entity<MapPathLocationType>());
            new PlayerMap(modelBuilder.Entity<Player>());
            new PlayerGenderMap(modelBuilder.Entity<PlayerGender>());
            new PlayerTypeMap(modelBuilder.Entity<PlayerType>());
            new SpecialtyTokenMap(modelBuilder.Entity<SpecialtyToken>());
            new SpecialtyTokenGamePlayerTypeMap(modelBuilder.Entity<SpecialtyTokenGamePlayerType>());
            new TransportModeMap(modelBuilder.Entity<TransportMode>());

            // modelBuilder.Entity<PlayerGender>().HasData(new Blog {BlogId = 1, Url = "http://sample.com"});
            // Playergender.SeedEnumValues<Playergender, PlayerGenderEnum>(@enum => @enum);
            // Playertype.SeedEnumValues<Playertype, PlayerTypeEnum>(@enum => @enum);
            // Gameplayertype.SeedEnumValues<Gameplayertype, GamePlayerTypeEnum>(@enum => @enum);
            // Gamestatus.SeedEnumValues<Gamestatus, GameStatusEnum>(@enum => @enum);
            // Mapvertexlocationtype.SeedEnumValues<Mapvertexlocationtype, MapVertexLocationTypeEnum>(@enum => @enum);
            // Specialtytoken.SeedEnumValues<Specialtytoken, SpecialtyTokenEnum>(@enum => @enum);
            
            // SaveChanges();
        }
    }
}
