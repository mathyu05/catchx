using CatchXApi.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CatchXApi.Data.Maps
{
    public class SpecialtyTokenGamePlayerTypeMap
    {
        public SpecialtyTokenGamePlayerTypeMap(EntityTypeBuilder<SpecialtyTokenGamePlayerType> entity)
        {
            entity.ToTable("SpecialtyTokenGamePlayerType");

            entity.HasKey(e => new { e.SpecialtyTokenId, e.GamePlayerTypeId })
                .HasName("PRIMARY");

            entity.Property(e => e.SpecialtyTokenId)
                .HasColumnName("SpecialtyTokenId")
                .HasColumnType("TINYINT UNSIGNED")
                .IsRequired();

            entity.Property(e => e.GamePlayerTypeId)
                .HasColumnName("GamePlayerTypeId")
                .HasColumnType("TINYINT UNSIGNED")
                .IsRequired();

            entity.HasOne(d => d.GamePlayerType)
                .WithMany(p => p.SpecialtyTokenGamePlayerType)
                .HasForeignKey(d => d.GamePlayerTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_GPT_STGPT_GPTId");

            entity.HasOne(d => d.SpecialtyToken)
                .WithMany(p => p.SpecialtyTokenGamePlayerType)
                .HasForeignKey(d => d.SpecialtyTokenId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ST_STGPT_STId");

            entity.HasIndex(e => e.GamePlayerTypeId)
                .HasName("FK_GPT_STGPT_GPTId");
        }
    }
}