using CatchXApi.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CatchXApi.Data.Maps
{
    public class GamePlayerMoveMap
    {
        public GamePlayerMoveMap(EntityTypeBuilder<GamePlayerMove> entity)
        {
            entity.ToTable("GamePlayerMove");

            entity.HasKey(e => e.Id)
                .HasName("PRIMARY");

            entity.Property(e => e.Id)
                .HasColumnName("Id")
                .HasColumnType("SMALLINT UNSIGNED")
                .IsRequired()
                .ValueGeneratedOnAdd();

            entity.Property(e => e.GameId)
                .HasColumnName("GameId")
                .HasColumnType("INT UNSIGNED")
                .IsRequired();

            entity.Property(e => e.PlayerId)
                .HasColumnName("PlayerId")
                .HasColumnType("INT UNSIGNED")
                .IsRequired();

            entity.Property(e => e.LocationId)
                .HasColumnName("LocationId")
                .HasColumnType("INT UNSIGNED")
                .IsRequired();

            entity.Property(e => e.TransportModeId)
                .HasColumnName("TransportModeId")
                .HasColumnType("SMALLINT UNSIGNED")
                .IsRequired();

            entity.Property(e => e.PlayerPreviousMoveId)
                .HasColumnName("PlayerPreviousMoveId")
                .HasColumnType("SMALLINT UNSIGNED")
                .IsRequired(false);

            entity.HasOne(d => d.Game)
                .WithMany(p => p.GamePlayerMove)
                .HasForeignKey(d => d.GameId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Game_GamePlayerMove_GameId");

            entity.HasOne(d => d.Location)
                .WithMany(p => p.GamePlayerMove)
                .HasForeignKey(d => d.LocationId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Location_GamePlayerMove_LocationId");

            entity.HasOne(d => d.Player)
                .WithMany(p => p.GamePlayerMove)
                .HasForeignKey(d => d.PlayerId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Player_GamePlayerMove_PlayerId");

            entity.HasOne(d => d.PlayerPreviousMove)
                .WithMany(p => p.InversePlayerPreviousMove)
                .HasForeignKey(d => d.PlayerPreviousMoveId)
                .HasConstraintName("FK_GamePlayerMove_GamePlayerMove_PlayerPreviousMoveId");

            entity.HasOne(d => d.TransportMode)
                .WithMany(p => p.GamePlayerMove)
                .HasForeignKey(d => d.TransportModeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_TransportMode_GamePlayerMove_TransportModeId");

            entity.HasIndex(e => e.GameId)
                .HasName("FK_Game_GamePlayerMove_GameId");

            entity.HasIndex(e => e.LocationId)
                .HasName("FK_Location_GamePlayerMove_LocationId");

            entity.HasIndex(e => e.PlayerId)
                .HasName("FK_Player_GamePlayerMove_PlayerId");

            entity.HasIndex(e => e.PlayerPreviousMoveId)
                .HasName("FK_GamePlayerMove_GamePlayerMove_PlayerPreviousMoveId");

            entity.HasIndex(e => e.TransportModeId)
                .HasName("FK_TransportMode_GamePlayerMove_TransportModeId");
        }
    }
}