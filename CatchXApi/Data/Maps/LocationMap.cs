using CatchXApi.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CatchXApi.Data.Maps
{
    public class LocationMap
    {
        public LocationMap(EntityTypeBuilder<Location> entity)
        {
            entity.ToTable("Location");

            entity.HasKey(e => e.Id)
                .HasName("PRIMARY");

            entity.Property(e => e.Id)
                .HasColumnName("Id")
                .HasColumnType("INT UNSIGNED")
                .IsRequired();

            entity.Property(e => e.Name)
                .HasColumnName("Name")
                .HasColumnType("VARCHAR(30)")
                .HasMaxLength(30)
                .IsRequired()
                .ValueGeneratedOnAdd();

            entity.Property(e => e.Description)
                .HasColumnName("Description")
                .HasColumnType("VARCHAR(100)")
                .HasMaxLength(100)
                .IsRequired();

            entity.Property(e => e.Latitude)
                .HasColumnName("Latitude")
                .HasColumnType("DECIMAL(10, 8)")
                .IsRequired();

            entity.Property(e => e.Longitude)
                .HasColumnName("Longitude")
                .HasColumnType("DECIMAL(11, 8)")
                .IsRequired();
        }
    }
}