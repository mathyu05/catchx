using CatchXApi.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CatchXApi.Data.Maps
{
    public class GamePlayerMap
    {
        public GamePlayerMap(EntityTypeBuilder<GamePlayer> entity)
        {
            entity.ToTable("GamePlayer");

            entity.HasKey(e => new { e.GameId, e.PlayerId })
                .HasName("PRIMARY");

            entity.Property(e => e.GameId)
                .HasColumnName("GameId")
                .HasColumnType("INT UNSIGNED")
                .IsRequired();

            entity.Property(e => e.PlayerId)
                .HasColumnName("PlayerId")
                .HasColumnType("INT UNSIGNED")
                .IsRequired();

            entity.Property(e => e.GamePlayerTypeId)
                .HasColumnName("GamePlayerTypeId")
                .HasColumnType("TINYINT UNSIGNED")
                .IsRequired();

            entity.Property(e => e.OrderOfPlay)
                .HasColumnName("OrderOfPlay")
                .HasColumnType("TINYINT UNSIGNED")
                .IsRequired(false);

            entity.Property(e => e.StartingLocationId)
                .HasColumnName("StartingLocationId")
                .HasColumnType("INT UNSIGNED")
                .IsRequired(false);

            entity.HasOne(d => d.Game)
                .WithMany(p => p.GamePlayer)
                .HasForeignKey(d => d.GameId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Game_GamePlayer_GameId");

            entity.HasOne(d => d.GamePlayerType)
                .WithMany(p => p.GamePlayer)
                .HasForeignKey(d => d.GamePlayerTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_GamePlayerType_GamePlayer_GamePlayerTypeId");

            entity.HasOne(d => d.Player)
                .WithMany(p => p.GamePlayer)
                .HasForeignKey(d => d.PlayerId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Player_GamePlayer_PlayerId");

            entity.HasOne(d => d.StartingLocation)
                .WithMany(p => p.GamePlayer)
                .HasForeignKey(d => d.StartingLocationId)
                .HasConstraintName("FK_Location_GamePlayer_StartingLocationId");

            entity.HasIndex(e => e.GamePlayerTypeId)
                .HasName("FK_GamePlayerType_GamePlayer_GamePlayerTypeId");

            entity.HasIndex(e => e.PlayerId)
                .HasName("FK_Player_GamePlayer_PlayerId");

            entity.HasIndex(e => e.StartingLocationId)
                .HasName("FK_Location_GamePlayer_StartingLocationId");
        }
    }
}