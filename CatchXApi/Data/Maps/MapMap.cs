using CatchXApi.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CatchXApi.Data.Maps
{
    public class MapMap
    {
        public MapMap(EntityTypeBuilder<Map> entity)
        {
            entity.ToTable("Map");

            entity.HasKey(e => e.Id)
                .HasName("PRIMARY");

            entity.Property(e => e.Id)
                .HasColumnName("Id")
                .HasColumnType("INT UNSIGNED")
                .IsRequired()
                .ValueGeneratedOnAdd();

            entity.Property(e => e.Name)
                .HasColumnName("Name")
                .HasColumnType("VARCHAR(30)")
                .HasMaxLength(30)
                .IsRequired();

            entity.Property(e => e.Description)
                .HasColumnName("Description")
                .HasColumnType("VARCHAR(100)")
                .HasMaxLength(100)
                .IsRequired();

            entity.Property(e => e.CreatorPlayerId)
                .HasColumnName("CreatorPlayerId")
                .HasColumnType("INT UNSIGNED")
                .IsRequired(false);

            entity.Property(e => e.CreateDate)
                .HasColumnName("CreateDate")
                .HasColumnType("DATETIME(2)")
                .IsRequired();

            entity.HasOne(d => d.CreatorPlayer)
                .WithMany(p => p.Map)
                .HasForeignKey(d => d.CreatorPlayerId)
                .HasConstraintName("FK_Player_Map_CreatorPlayerId");

            entity.HasIndex(e => e.CreatorPlayerId)
                .HasName("FK_Player_Map_CreatorPlayerId");

            entity.HasIndex(e => e.Name)
                .HasName("UC_Map_Name")
                .IsUnique();
        }
    }
}