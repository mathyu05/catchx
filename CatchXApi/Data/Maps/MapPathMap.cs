using CatchXApi.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CatchXApi.Data.Maps
{
    public class MapPathMap
    {
        public MapPathMap(EntityTypeBuilder<MapPath> entity)
        {
            entity.ToTable("MapPath");

            entity.HasKey(e => e.Id)
                .HasName("PRIMARY");

            entity.Property(e => e.Id)
                .HasColumnName("Id")
                .HasColumnType("INT UNSIGNED")
                .IsRequired()
                .ValueGeneratedOnAdd();

            entity.Property(e => e.MapId)
                .HasColumnName("MapId")
                .HasColumnType("INT UNSIGNED")
                .IsRequired();

            entity.Property(e => e.TransportModeId)
                .HasColumnName("TransportModeId")
                .HasColumnType("SMALLINT UNSIGNED")
                .IsRequired();

            entity.HasOne(d => d.Map)
                .WithMany(p => p.MapPath)
                .HasForeignKey(d => d.MapId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Map_MapPath_MapId");

            entity.HasOne(d => d.TransportMode)
                .WithMany(p => p.MapPath)
                .HasForeignKey(d => d.TransportModeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_TransportMode_MapPath_TransportModeId");

            entity.HasIndex(e => e.MapId)
                .HasName("FK_Map_MapPath_MapId");

            entity.HasIndex(e => e.TransportModeId)
                .HasName("FK_TransportMode_MapPath_TransportModeId");
        }
    }
}