using CatchXApi.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CatchXApi.Data.Maps
{
    public class MapPathLocationMap
    {
        public MapPathLocationMap(EntityTypeBuilder<MapPathLocation> entity)
        {
            entity.ToTable("MapPathLocation");

            entity.HasKey(e => new { e.MapPathId, e.LocationId, e.MapPathLocationTypeId })
                .HasName("PRIMARY");

            entity.Property(e => e.MapPathId)
                .HasColumnName("MapPathId")
                .HasColumnType("INT UNSIGNED")
                .IsRequired();

            entity.Property(e => e.LocationId)
                .HasColumnName("LocationId")
                .HasColumnType("INT UNSIGNED")
                .IsRequired();

            entity.Property(e => e.MapPathLocationTypeId)
                .HasColumnName("MapPathLocationTypeId")
                .HasColumnType("TINYINT UNSIGNED")
                .IsRequired();

            entity.HasOne(d => d.Location)
                .WithMany(p => p.MapPathLocation)
                .HasForeignKey(d => d.LocationId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Location_MapPathLocation_LocationId");

            entity.HasOne(d => d.MapPath)
                .WithMany(p => p.MapPathLocation)
                .HasForeignKey(d => d.MapPathId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_MapPath_MapPathLocation_MapPathId");

            entity.HasOne(d => d.MapPathLocationType)
                .WithMany(p => p.MapPathLocation)
                .HasForeignKey(d => d.MapPathLocationTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_MPLT_MPL_MPLTId");

            entity.HasIndex(e => e.LocationId)
                .HasName("FK_Location_MapPathLocation_LocationId");

            entity.HasIndex(e => e.MapPathLocationTypeId)
                .HasName("FK_MPLT_MPL_MPLTId");
        }
    }
}