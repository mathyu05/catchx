using CatchXApi.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CatchXApi.Data.Maps
{
    public class TransportModeMap
    {
        public TransportModeMap(EntityTypeBuilder<TransportMode> entity)
        {
            entity.ToTable("TransportMode");

            entity.HasKey(e => e.Id)
                .HasName("PRIMARY");

            entity.Property(e => e.Id)
                .HasColumnName("Id")
                .HasColumnType("SMALLINT UNSIGNED")
                .IsRequired()
                .ValueGeneratedOnAdd();

            entity.Property(e => e.Name)
                .HasColumnName("Name")
                .HasColumnType("VARCHAR(20)")
                .HasMaxLength(20)
                .IsRequired();

            entity.HasIndex(e => e.Name)
                .HasName("UC_TransportMode_Name")
                .IsUnique();
        }
    }
}