using CatchXApi.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CatchXApi.Data.Maps
{
    public class MapLocationMap
    {
        public MapLocationMap(EntityTypeBuilder<MapLocation> entity)
        {
            entity.ToTable("MapLocation");

            entity.HasKey(e => new { e.MapId, e.LocationId })
                .HasName("PRIMARY");

            entity.Property(e => e.MapId)
                .HasColumnName("MapId")
                .HasColumnType("INT UNSIGNED")
                .IsRequired();

            entity.Property(e => e.LocationId)
                .HasColumnName("LocationId")
                .HasColumnType("INT UNSIGNED")
                .IsRequired();

            entity.HasOne(d => d.Location)
                .WithMany(p => p.MapLocation)
                .HasForeignKey(d => d.LocationId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Location_MapLocation_LocationId");

            entity.HasOne(d => d.Map)
                .WithMany(p => p.MapLocation)
                .HasForeignKey(d => d.MapId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Map_MapLocation_MapId");

            entity.HasIndex(e => e.LocationId)
                .HasName("FK_Location_MapLocation_LocationId");
        }
    }
}