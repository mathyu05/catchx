using System;
using System.Linq;
using CatchXApi.Data.Enums;
using CatchXApi.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CatchXApi.Data.Maps
{
    public class PlayerGenderMap
    {
        public PlayerGenderMap(EntityTypeBuilder<PlayerGender> entity)
        {
            entity.ToTable("PlayerGender");

            entity.HasKey(e => e.Id)
                .HasName("PRIMARY");

            entity.Property(e => e.Id)
                .HasColumnName("Id")
                .HasColumnType("TINYINT UNSIGNED")
                .IsRequired();

            entity.Property(e => e.Name)
                .HasColumnName("Name")
                .HasColumnType("VARCHAR(20)")
                .HasMaxLength(20)
                .IsRequired();

            entity.HasIndex(e => e.Name)
                .HasName("UC_PlayerGender_Name")
                .IsUnique();

            entity.HasData(
                Enum.GetValues(typeof(PlayerGenderEnum))
                    .Cast<PlayerGenderEnum>()
                    .Select(e => new PlayerGender { Id = (byte)e, Name = e.ToString() })
            );
        }
    }
}