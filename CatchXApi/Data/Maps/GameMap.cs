using CatchXApi.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CatchXApi.Data.Maps
{
    public class GameMap
    {
        public GameMap(EntityTypeBuilder<Game> entity)
        {
            entity.ToTable("Game");

            entity.HasKey(e => e.Id)
                .HasName("PRIMARY");

            entity.Property(e => e.Id)
                .HasColumnName("Id")
                .HasColumnType("INT UNSIGNED")
                .IsRequired()
                .ValueGeneratedOnAdd();

            entity.Property(e => e.Name)
                .HasColumnName("Name")
                .HasColumnType("VARCHAR(30)")
                .HasMaxLength(30)
                .IsRequired();

            entity.Property(e => e.MapId)
                .HasColumnName("MapId")
                .HasColumnType("INT UNSIGNED")
                .IsRequired();

            entity.Property(e => e.NumberOfPlayers)
                .HasColumnName("NumberOfPlayers")
                .HasColumnType("TINYINT UNSIGNED")
                .IsRequired();

            entity.Property(e => e.GameStatusId)
                .HasColumnName("GameStatusId")
                .HasColumnType("TINYINT UNSIGNED")
                .IsRequired();

            entity.Property(e => e.CreatorPlayerId)
                .HasColumnName("CreatorPlayerId")
                .HasColumnType("INT UNSIGNED")
                .IsRequired(false);

            entity.Property(e => e.CreateDate)
                .HasColumnName("CreateDate")
                .HasColumnType("DATETIME(2)")
                .IsRequired();

            entity.HasOne(d => d.CreatorPlayer)
                .WithMany(p => p.Game)
                .HasForeignKey(d => d.CreatorPlayerId)
                .HasConstraintName("FK_Player_Game_CreatorPlayerId");

            entity.HasOne(d => d.GameStatus)
                .WithMany(p => p.Game)
                .HasForeignKey(d => d.GameStatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_GameStatus_Game_GameStatusId");

            entity.HasOne(d => d.Map)
                .WithMany(p => p.Game)
                .HasForeignKey(d => d.MapId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Map_Game_MapId");

            entity.HasIndex(e => e.CreatorPlayerId)
                .HasName("FK_Player_Game_CreatorPlayerId");

            entity.HasIndex(e => e.GameStatusId)
                .HasName("FK_GameStatus_Game_GameStatusId");

            entity.HasIndex(e => e.MapId)
                .HasName("FK_Map_Game_MapId");

            entity.HasIndex(e => e.Name)
                .HasName("UC_Game_Name")
                .IsUnique();
        }
    }
}