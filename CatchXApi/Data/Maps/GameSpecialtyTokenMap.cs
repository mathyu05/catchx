using CatchXApi.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CatchXApi.Data.Maps
{
    public class GameSpecialtyTokenMap
    {
        public GameSpecialtyTokenMap(EntityTypeBuilder<GameSpecialtyToken> entity)
        {
            entity.ToTable("GameSpecialtyToken");

            entity.HasKey(e => new { e.GameId, e.SpecialtyTokenId })
                .HasName("PRIMARY");

            entity.Property(e => e.GameId)
                .HasColumnName("GameId")
                .HasColumnType("INT UNSIGNED")
                .IsRequired();

            entity.Property(e => e.SpecialtyTokenId)
                .HasColumnName("SpecialtyTokenId")
                .HasColumnType("TINYINT UNSIGNED")
                .IsRequired();

            entity.Property(e => e.DetectiveStartingTokens)
                .HasColumnName("DetectiveStartingTokens")
                .HasColumnType("TINYINT UNSIGNED")
                .IsRequired();

            entity.Property(e => e.FelonStartingTokens)
                .HasColumnName("FelonStartingTokens")
                .HasColumnType("TINYINT UNSIGNED")
                .IsRequired();

            entity.HasOne(d => d.Game)
                .WithMany(p => p.GameSpecialtyToken)
                .HasForeignKey(d => d.GameId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Game_GameSpecialityToken_GameId");

            entity.HasOne(d => d.SpecialtyToken)
                .WithMany(p => p.GameSpecialtyToken)
                .HasForeignKey(d => d.SpecialtyTokenId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_SpecialtyToken_GST_STI");

            entity.HasIndex(e => e.SpecialtyTokenId)
                .HasName("FK_SpecialtyToken_GST_STI");
        }
    }
}