using CatchXApi.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CatchXApi.Data.Maps
{
    public class PlayerMap
    {
        public PlayerMap(EntityTypeBuilder<Player> entity)
        {
                entity.ToTable("Player");

                entity.HasKey(e => e.Id)
                .HasName("PRIMARY");

                entity.Property(e => e.Id)
                    .HasColumnName("Id")
                    .HasColumnType("INT UNSIGNED")
                    .IsRequired()
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Name)
                    .HasColumnName("Name")
                    .HasColumnType("VARCHAR(20)")
                    .HasMaxLength(20)
                    .IsRequired();

                entity.Property(e => e.Email)
                    .HasColumnName("Email")
                    .HasColumnType("VARCHAR(100)")
                    .HasMaxLength(100)
                    .IsRequired();

                entity.Property(e => e.Description)
                    .HasColumnName("Description")
                    .HasColumnType("VARCHAR(100)")
                    .HasMaxLength(100)
                    .IsRequired();

                entity.Property(e => e.PasswordHash)
                    .HasColumnName("PasswordHash")
                    .HasColumnType("BLOB")
                    .IsRequired();

                entity.Property(e => e.PasswordSalt)
                    .HasColumnName("PasswordSalt")
                    .HasColumnType("BLOB")
                    .IsRequired();

                entity.Property(e => e.PlayerTypeId)
                    .HasColumnName("PlayerTypeId")
                    .HasColumnType("TINYINT UNSIGNED")
                    .IsRequired();

                entity.Property(e => e.PlayerGenderId)
                    .HasColumnName("PlayerGenderId")
                    .HasColumnType("TINYINT UNSIGNED")
                    .IsRequired();

                entity.Property(e => e.GamesCompleted)
                    .HasColumnName("GamesCompleted")
                    .HasColumnType("INT UNSIGNED")
                    .IsRequired();

                entity.Property(e => e.CreateDate)
                    .HasColumnName("CreateDate")
                    .HasColumnType("DATETIME(2)")
                    .IsRequired();

                entity.Property(e => e.DeleteDate)
                    .HasColumnName("DeleteDate")
                    .HasColumnType("DATETIME(2)")
                    .IsRequired(false);

                entity.HasOne(d => d.PlayerGender)
                    .WithMany(p => p.Player)
                    .HasForeignKey(d => d.PlayerGenderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PlayerGender_Player_PlayerGenderId");

                entity.HasOne(d => d.PlayerType)
                    .WithMany(p => p.Player)
                    .HasForeignKey(d => d.PlayerTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PlayerType_Player_PlayerTypeId");

                entity.HasIndex(e => e.Email)
                    .HasName("UC_Player_Email")
                    .IsUnique();

                entity.HasIndex(e => e.Name)
                    .HasName("UC_Player_Name")
                    .IsUnique();

                entity.HasIndex(e => e.PlayerGenderId)
                    .HasName("FK_PlayerGender_Player_PlayerGenderId");

                entity.HasIndex(e => e.PlayerTypeId)
                    .HasName("FK_PlayerType_Player_PlayerTypeId");
        }
    }
}