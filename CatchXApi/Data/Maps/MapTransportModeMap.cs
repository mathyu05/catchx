using CatchXApi.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CatchXApi.Data.Maps
{
    public class MapTransportModeMap
    {
        public MapTransportModeMap(EntityTypeBuilder<MapTransportMode> entity)
        {
            entity.ToTable("MapTransportMode");

            entity.HasKey(e => new { e.MapId, e.TransportModeId })
                .HasName("PRIMARY");

            entity.Property(e => e.MapId)
                .HasColumnName("MapId")
                .HasColumnType("INT UNSIGNED")
                .IsRequired();

            entity.Property(e => e.TransportModeId)
                .HasColumnName("TransportModeId")
                .HasColumnType("SMALLINT UNSIGNED")
                .IsRequired();

            entity.Property(e => e.DetectiveStartingTickets)
                .HasColumnName("DetectiveStartingTickets")
                .HasColumnType("TINYINT UNSIGNED")
                .IsRequired();

            entity.Property(e => e.FelonStartingTickets)
                .HasColumnName("FelonStartingTickets")
                .HasColumnType("TINYINT UNSIGNED")
                .IsRequired();

            entity.HasOne(d => d.Map)
                .WithMany(p => p.MapTransportMode)
                .HasForeignKey(d => d.MapId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Map_MapTransportMode_MapId");

            entity.HasOne(d => d.TransportMode)
                .WithMany(p => p.MapTransportMode)
                .HasForeignKey(d => d.TransportModeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_TransportMode_MapTransportMode_TransportModeId");

            entity.HasIndex(e => e.TransportModeId)
                .HasName("FK_TransportMode_MapTransportMode_TransportModeId");
        }
    }
}