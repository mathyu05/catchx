using System;
using System.Linq;
using CatchXApi.Data.Enums;
using CatchXApi.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CatchXApi.Data.Maps
{
    public class SpecialtyTokenMap
    {
        public SpecialtyTokenMap(EntityTypeBuilder<SpecialtyToken> entity)
        {
            entity.ToTable("SpecialtyToken");

            entity.HasKey(e => e.Id)
            .HasName("PRIMARY");

            entity.Property(e => e.Id)
                .HasColumnType("Id")
                .HasColumnType("TINYINT UNSIGNED")
                .IsRequired();

            entity.Property(e => e.Name)
                .HasColumnType("Name")
                .HasColumnType("VARCHAR(20)")
                .HasMaxLength(20)
                .IsRequired();

            entity.HasIndex(e => e.Name)
                .HasName("UC_SpecialtyToken_Name")
                .IsUnique();

            entity.HasData(
                Enum.GetValues(typeof(SpecialtyTokenEnum))
                    .Cast<SpecialtyTokenEnum>()
                    .Select(e => new SpecialtyToken { Id = (byte)e, Name = e.ToString() })
            );
        }
    }
}