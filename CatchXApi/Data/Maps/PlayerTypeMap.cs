using System;
using System.Linq;
using CatchXApi.Data.Enums;
using CatchXApi.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CatchXApi.Data.Maps
{
    public class PlayerTypeMap
    {
        public PlayerTypeMap(EntityTypeBuilder<PlayerType> entity)
        {
            entity.ToTable("PlayerType");

            entity.HasKey(e => e.Id)
            .HasName("PRIMARY");

            entity.Property(e => e.Id)
                .HasColumnName("Id")
                .HasColumnType("TINYINT UNSIGNED")
                .IsRequired();

            entity.Property(e => e.Name)
                .HasColumnName("Name")
                .HasColumnType("VARCHAR(20)")
                .HasMaxLength(20)
                .IsRequired();

            entity.HasIndex(e => e.Name)
                .HasName("UC_PlayerType_Name")
                .IsUnique();

            entity.HasData(
                Enum.GetValues(typeof(PlayerTypeEnum))
                    .Cast<PlayerTypeEnum>()
                    .Select(e => new PlayerType { Id = (byte)e, Name = e.ToString() })
            );
        }
    }
} 