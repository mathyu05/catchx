using System;
using System.Linq;
using CatchXApi.Data.Enums;
using CatchXApi.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CatchXApi.Data.Maps
{
    public class GameStatusMap
    {
        public GameStatusMap(EntityTypeBuilder<GameStatus> entity)
        {
            entity.ToTable("GameStatus");

            entity.HasKey(e => e.Id)
                .HasName("PRIMARY");

            entity.Property(e => e.Id)
                .HasColumnName("Id")
                .HasColumnType("TINYINT UNSIGNED")
                .IsRequired();

            entity.Property(e => e.Name)
                .HasColumnName("Name")
                .HasColumnType("VARCHAR(20)")
                .HasMaxLength(20)
                .IsRequired();

            entity.HasIndex(e => e.Name)
                .HasName("UC_GameStatus_Name")
                .IsUnique();

            entity.HasData(
                Enum.GetValues(typeof(GameStatusEnum))
                    .Cast<GameStatusEnum>()
                    .Select(e => new GameStatus { Id = (byte)e, Name = e.ToString() })
            );
        }
    }
}