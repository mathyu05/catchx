namespace CatchXApi.Data.Enums
{
    public enum GamePlayerTypeEnum
    {
        Unassigned = 0,
        Detective = 1,
        Felon = 2
    }
}