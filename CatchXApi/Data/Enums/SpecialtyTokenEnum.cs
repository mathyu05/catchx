namespace CatchXApi.Data.Enums
{
    public enum SpecialtyTokenEnum
    {
        BlackTicket = 0,
        DoubleMove = 1
    }
}