namespace CatchXApi.Data.Enums
{
    public enum PlayerTypeEnum
    {
        Human = 0,
        CPU = 1
    }
}