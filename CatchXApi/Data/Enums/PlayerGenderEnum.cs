namespace CatchXApi.Data.Enums
{
    public enum PlayerGenderEnum
    {
        Unknown = 0,
        Male = 1,
        Female = 2
    }
}