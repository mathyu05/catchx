namespace CatchXApi.Data.Enums
{
    public enum GameStatusEnum
    {
        Creating = 0,
        Created = 1,
        InProgress = 2,
        Complete = 3
    }
}