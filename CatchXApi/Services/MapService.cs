using System;
using System.Collections.Generic;
using System.Security.Claims;
using CatchXApi.Data;
using CatchXApi.Data.Enums;
using CatchXApi.Data.Models;
using Microsoft.AspNetCore.Http;

namespace CatchXApi.Services
{
    public class MapService : IMapService
    {
        private CatchXContext _context;
        private uint _userId;

        public MapService(CatchXContext context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;

            _userId = Convert.ToUInt32(httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
        }

        public IEnumerable<Map> GetAll()
        {
            throw new System.NotImplementedException();
        }

        public Map GetById(uint id)
        {
            throw new System.NotImplementedException();
        }

        public Map Create(Map map)
        {
            map.CreatorPlayerId = _userId;
            map.CreateDate = DateTime.UtcNow;

            _context.Set<Map>().Add(map);
            _context.SaveChanges();

            return map;
        }

        public MapTransportMode AddTransportMode(MapTransportMode mapTransportMode)
        {
            _context.Set<MapTransportMode>().Add(mapTransportMode);
            _context.SaveChanges();

            return mapTransportMode;
        }

        public MapLocation AddLocation(MapLocation mapLocation)
        {
            _context.Set<MapLocation>().Add(mapLocation);
            _context.SaveChanges();

            return mapLocation;
        }

        public MapPath AddPath(MapPath mapPath, uint locationOneId, uint locationTwoId)
        {
            _context.Set<MapPath>().Add(mapPath);
            _context.SaveChanges();

            _context.Set<MapPathLocation>().AddRange(
                new MapPathLocation
                {
                    MapPathId = mapPath.Id,
                    LocationId = locationOneId,
                    MapPathLocationTypeId = (byte)MapPathLocationTypeEnum.First
                },
                new MapPathLocation
                {
                    MapPathId = mapPath.Id,
                    LocationId = locationTwoId,
                    MapPathLocationTypeId = (byte)MapPathLocationTypeEnum.Second
                }
            );
            _context.SaveChanges();

            return mapPath;
        }

        public void Update(Map map)
        {
            throw new System.NotImplementedException();
        }

        public void Delete(uint id)
        {
            throw new System.NotImplementedException();
        }
    }
}