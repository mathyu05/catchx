using System.Collections.Generic;
using CatchXApi.Exceptions;
using CatchXApi.Data.Models;
using CatchXApi.Data;

namespace CatchXApi.Services
{
    public class LocationService : ILocationService
    {
        private CatchXContext _context;

        public LocationService(CatchXContext context)
        {
            _context = context;
        }

        public IEnumerable<Location> GetAll()
        {
            return _context.Set<Location>();
        }

        public Location GetById(uint id)
        {
            return _context.Set<Location>().Find(id);
        }

        public Location Create(Location location)
        {
            _context.Set<Location>().Add(location);
            _context.SaveChanges();

            return location;
        }

        public void Update(Location locationParam)
        {
            Location location = _context.Set<Location>().Find(locationParam.Id);

            if (location == null)
            {
                throw new AppException("Location not found");
            }

            location.Name = locationParam.Name;
            location.Description = locationParam.Description;

            _context.Set<Location>().Update(location);
            _context.SaveChanges();
        }

        public void Delete(uint id)
        {
            // TODO Delete
        }
    }
}