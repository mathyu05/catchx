using System.Collections.Generic;
using CatchXApi.Data.Models;

namespace CatchXApi.Services
{
    public interface IPlayerService
    {
        Player Authenticate(string name, string password);
        IEnumerable<Player> GetAll();
        Player GetById(uint id);
        Player Create(Player player, string password);
        void Update(Player player, string password = null);
        void Delete(uint id);
    }
}