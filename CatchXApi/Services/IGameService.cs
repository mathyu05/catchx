using System.Collections.Generic;
using CatchXApi.Data.Models;

namespace CatchXApi.Services
{
    public interface IGameService
    {
        IEnumerable<Game> GetAll();
        Game GetById(uint id);
        Game Create(Game game);
        void Update(Game game);
        void Delete(uint id);
        void AddSpecialtyToken(GameSpecialtyToken gameSpecialtyToken);
        void OpenLobby();
        void Start(uint id);
        void Join(uint gameId, uint playerId);
        void Leave(uint gameId, uint playerId);
    }
}