using System.Collections.Generic;
using CatchXApi.Data.Models;

namespace CatchXApi.Services
{
    public interface ILocationService
    {
        IEnumerable<Location> GetAll();
        Location GetById(uint id);
        Location Create(Location location);
        void Update(Location location);
        void Delete(uint id);
    }
}