using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using CatchXApi.Data;
using CatchXApi.Data.Enums;
using CatchXApi.Data.Models;
using Microsoft.AspNetCore.Http;

namespace CatchXApi.Services
{
    public class GameService : IGameService
    {
        private CatchXContext _context;
        private uint _userId;
        public GameService(CatchXContext context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;

            _userId = Convert.ToUInt32(httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
        }

        public Game Create(Game game)
        {
            game.CreatorPlayerId = _userId;
            game.CreateDate = DateTime.UtcNow;
            game.GameStatusId = (byte)GameStatusEnum.Creating;

            _context.Set<Game>().Add(game);
            _context.SaveChanges();

            return game;
        }

        public void AddSpecialtyToken(GameSpecialtyToken gameSpecialtyToken)
        {
            _context.Set<GameSpecialtyToken>().Add(gameSpecialtyToken);
            _context.SaveChanges();
        }

        public void OpenLobby()
        {
            // _context.Set<Game>().Update(game);
            // _context.SaveChanges();
        }

        public void Start(uint id)
        {
            Game game = _context.Set<Game>().Find(id);

            game.GameStatusId = (byte)GameStatusEnum.InProgress;

            // Assign Game Player Types
            var gamePlayers = _context.Set<GamePlayer>().Where(gp => gp.GameId == id);

            foreach (var gamePlayer in gamePlayers)
            {
                gamePlayer.GamePlayerTypeId = (byte)GamePlayerTypeEnum.Detective; // Randomise
                gamePlayer.StartingLocationId = 1; // Randomise
                gamePlayer.OrderOfPlay = 1; // Randomise
            }

            _context.Set<GamePlayer>().UpdateRange(gamePlayers);

            _context.Set<Game>().Update(game);
            _context.SaveChanges();
        }

        public void Join(uint gameId, uint playerId)
        {
            _context.Set<GamePlayer>().Add(
                new GamePlayer
                {
                    GameId = gameId,
                    PlayerId = playerId,
                    GamePlayerTypeId = (byte)GamePlayerTypeEnum.Unassigned
                }
            );
            _context.SaveChanges();
        }

        public void Leave(uint gameId, uint playerId)
        {
            GamePlayer gamePlayer = _context.Set<GamePlayer>().Find(gameId, playerId);

            if (gamePlayer == null)
            {
                return;
            }

            _context.Set<GamePlayer>().Remove(gamePlayer);

            _context.SaveChanges();
        }

        public void Delete(uint id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Game> GetAll()
        {
            throw new NotImplementedException();
        }

        public Game GetById(uint id)
        {
            throw new NotImplementedException();
        }

        public void Update(Game map)
        {
            throw new NotImplementedException();
        }
    }
}