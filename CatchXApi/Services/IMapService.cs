using System.Collections.Generic;
using CatchXApi.Data.Models;

namespace CatchXApi.Services
{
    public interface IMapService
    {
        IEnumerable<Map> GetAll();
        Map GetById(uint id);
        Map Create(Map map);
        void Update(Map map);
        void Delete(uint id);
        MapTransportMode AddTransportMode(MapTransportMode mapTransportMode);
        MapLocation AddLocation(MapLocation mapLocation);
        MapPath AddPath(MapPath mapPath, uint locationOneId, uint locationTwoId);
    }
}