using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using CatchXApi.Exceptions;
using CatchXApi.Data.Models;
using CatchXApi.Data;

namespace CatchXApi.Services
{
    public class PlayerService : IPlayerService
    {
        private CatchXContext _context;

        public PlayerService(CatchXContext context)
        {
            _context = context;
        }

        public Player Authenticate(string name, string password)
        {
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(password))
            {
                return null;
            }

            Player player = _context.Set<Player>().SingleOrDefault(p => p.Name == name);

            if (player == null)
            {
                return null;
            }

            if (!VerifyPasswordHash(password, player.PasswordHash, player.PasswordSalt))
            {
                return null;
            }

            return player;
        }

        public Player Create(Player player, string password)
        {
            if (string.IsNullOrWhiteSpace(password))
            {
                throw new AppException("Password is required");
            }

            if (_context.Set<Player>().Any(p => p.Name == player.Name))
            {
                throw new AppException($"Name [{player.Name}] is already taken");
            }

            byte[] passwordHash;
            byte[] passwordSalt;

            CreatePasswordHash(password, out passwordHash, out passwordSalt);

            player.PasswordHash = passwordHash;
            player.PasswordSalt = passwordSalt;

            _context.Set<Player>().Add(player);
            _context.SaveChanges();

            return player;
        }

        public void Delete(uint id)
        {
            Player player = _context.Set<Player>().Find(id);

            if (player != null)
            {
                // TODO mark deleted?

                _context.SaveChanges();
            }
        }

        public IEnumerable<Player> GetAll()
        {
            return _context.Set<Player>();
        }

        public Player GetById(uint id)
        {
            return _context.Set<Player>().Find(id);
        }

        public void Update(Player playerParam, string password = null)
        {
            Player player = _context.Set<Player>().Find(playerParam.Id);

            if (player == null)
            {
                throw new AppException("Player not found");
            }

            if (playerParam.Name != player.Name)
            {
                if (_context.Set<Player>().Any(p => p.Name == playerParam.Name))
                {
                    throw new AppException($"Name [{playerParam.Name}] is already taken");
                }
            }

            // TODO Update properties...
            player.Name = playerParam.Name;

            if (!string.IsNullOrWhiteSpace(password))
            {
                byte[] passwordHash;
                byte[] passwordSalt;
                CreatePasswordHash(password, out passwordHash, out passwordSalt);

                player.PasswordHash = passwordHash;
                player.PasswordSalt = passwordSalt;
            }

            _context.Set<Player>().Update(player);
            _context.SaveChanges();
        }

        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null)
            {
                throw new ArgumentNullException("password");
            }

            if (string.IsNullOrWhiteSpace(password))
            {
                throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            }

            using (HMACSHA512 hmac = new HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
            }
        }

        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null)
            {
                throw new ArgumentNullException("password");
            }

            if (string.IsNullOrWhiteSpace(password))
            {
                throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            }

            if (storedHash.Length != 64)
            {
                throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            }

            if (storedSalt.Length != 128)
            {
                throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");
            }

            using (HMACSHA512 hmac = new HMACSHA512(storedSalt))
            {
                byte[] computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));

                for (int i = 0; i < computedHash.Length; ++i)
                {
                    if (computedHash[i] != storedHash[i])
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}