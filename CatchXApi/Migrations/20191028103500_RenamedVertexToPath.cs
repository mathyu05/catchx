﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CatchXApi.Migrations
{
    public partial class RenamedVertexToPath : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "mapvertexlocation");

            migrationBuilder.DropTable(
                name: "MapVertex");

            migrationBuilder.DropTable(
                name: "MapVertexLocationType");

            migrationBuilder.CreateTable(
                name: "MapPath",
                columns: table => new
                {
                    Id = table.Column<uint>(type: "INT UNSIGNED", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    MapId = table.Column<uint>(type: "INT UNSIGNED", nullable: false),
                    TransportModeId = table.Column<ushort>(type: "SMALLINT UNSIGNED", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Map_MapPath_MapId",
                        column: x => x.MapId,
                        principalTable: "Map",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TransportMode_MapPath_TransportModeId",
                        column: x => x.TransportModeId,
                        principalTable: "TransportMode",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MapPathLocationType",
                columns: table => new
                {
                    Id = table.Column<byte>(type: "TINYINT UNSIGNED", nullable: false),
                    Name = table.Column<string>(type: "VARCHAR(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MapPathLocation",
                columns: table => new
                {
                    MapPathId = table.Column<uint>(type: "INT UNSIGNED", nullable: false),
                    LocationId = table.Column<uint>(type: "INT UNSIGNED", nullable: false),
                    MapPathLocationTypeId = table.Column<byte>(type: "TINYINT UNSIGNED", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => new { x.MapPathId, x.LocationId, x.MapPathLocationTypeId });
                    table.ForeignKey(
                        name: "FK_Location_MapPathLocation_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Location",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MapPath_MapPathLocation_MapPathId",
                        column: x => x.MapPathId,
                        principalTable: "MapPath",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MPLT_MPL_MPLTId",
                        column: x => x.MapPathLocationTypeId,
                        principalTable: "MapPathLocationType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "FK_Map_MapPath_MapId",
                table: "MapPath",
                column: "MapId");

            migrationBuilder.CreateIndex(
                name: "FK_TransportMode_MapPath_TransportModeId",
                table: "MapPath",
                column: "TransportModeId");

            migrationBuilder.CreateIndex(
                name: "FK_Location_MapPathLocation_LocationId",
                table: "MapPathLocation",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "FK_MPLT_MPL_MPLTId",
                table: "MapPathLocation",
                column: "MapPathLocationTypeId");

            migrationBuilder.CreateIndex(
                name: "UC_MapPathLocationType_Name",
                table: "MapPathLocationType",
                column: "Name",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MapPathLocation");

            migrationBuilder.DropTable(
                name: "MapPath");

            migrationBuilder.DropTable(
                name: "MapPathLocationType");

            migrationBuilder.CreateTable(
                name: "MapVertex",
                columns: table => new
                {
                    Id = table.Column<uint>(type: "INT UNSIGNED", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    MapId = table.Column<uint>(type: "INT UNSIGNED", nullable: false),
                    TransportModeId = table.Column<ushort>(type: "SMALLINT UNSIGNED", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Map_MapVertex_MapId",
                        column: x => x.MapId,
                        principalTable: "Map",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TransportMode_MapVertex_TransportModeId",
                        column: x => x.TransportModeId,
                        principalTable: "TransportMode",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MapVertexLocationType",
                columns: table => new
                {
                    Id = table.Column<byte>(type: "TINYINT UNSIGNED", nullable: false),
                    Name = table.Column<string>(type: "VARCHAR(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "mapvertexlocation",
                columns: table => new
                {
                    MapVertexId = table.Column<uint>(type: "INT UNSIGNED", nullable: false),
                    LocationId = table.Column<uint>(type: "INT UNSIGNED", nullable: false),
                    MapVertexLocationTypeId = table.Column<byte>(type: "TINYINT UNSIGNED", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => new { x.MapVertexId, x.LocationId, x.MapVertexLocationTypeId });
                    table.ForeignKey(
                        name: "FK_Location_MapVertexLocation_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Location",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MapVertex_MapVertexLocation_MapVertexId",
                        column: x => x.MapVertexId,
                        principalTable: "MapVertex",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MVLT_MVL_MVLTId",
                        column: x => x.MapVertexLocationTypeId,
                        principalTable: "MapVertexLocationType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "FK_Map_MapVertex_MapId",
                table: "MapVertex",
                column: "MapId");

            migrationBuilder.CreateIndex(
                name: "FK_TransportMode_MapVertex_TransportModeId",
                table: "MapVertex",
                column: "TransportModeId");

            migrationBuilder.CreateIndex(
                name: "FK_Location_MapVertexLocation_LocationId",
                table: "mapvertexlocation",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "FK_MVLT_MVL_MVLTId",
                table: "mapvertexlocation",
                column: "MapVertexLocationTypeId");

            migrationBuilder.CreateIndex(
                name: "UC_MapVertexLocationType_Name",
                table: "MapVertexLocationType",
                column: "Name",
                unique: true);
        }
    }
}
