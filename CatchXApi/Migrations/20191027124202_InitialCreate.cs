﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CatchXApi.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GamePlayerType",
                columns: table => new
                {
                    Id = table.Column<byte>(type: "TINYINT UNSIGNED", nullable: false),
                    Name = table.Column<string>(type: "VARCHAR(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GameStatus",
                columns: table => new
                {
                    Id = table.Column<byte>(type: "TINYINT UNSIGNED", nullable: false),
                    Name = table.Column<string>(type: "VARCHAR(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Location",
                columns: table => new
                {
                    Id = table.Column<uint>(type: "INT UNSIGNED", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "VARCHAR(30)", maxLength: 30, nullable: false),
                    Description = table.Column<string>(type: "VARCHAR(100)", maxLength: 100, nullable: false),
                    Latitude = table.Column<decimal>(type: "DECIMAL(10, 8)", nullable: false),
                    Longitude = table.Column<decimal>(type: "DECIMAL(11, 8)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MapVertexLocationType",
                columns: table => new
                {
                    Id = table.Column<byte>(type: "TINYINT UNSIGNED", nullable: false),
                    Name = table.Column<string>(type: "VARCHAR(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PlayerGender",
                columns: table => new
                {
                    Id = table.Column<byte>(type: "TINYINT UNSIGNED", nullable: false),
                    Name = table.Column<string>(type: "VARCHAR(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PlayerType",
                columns: table => new
                {
                    Id = table.Column<byte>(type: "TINYINT UNSIGNED", nullable: false),
                    Name = table.Column<string>(type: "VARCHAR(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SpecialtyToken",
                columns: table => new
                {
                    Id = table.Column<byte>(type: "TINYINT UNSIGNED", nullable: false),
                    Name = table.Column<string>(type: "VARCHAR(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TransportMode",
                columns: table => new
                {
                    Id = table.Column<ushort>(type: "SMALLINT UNSIGNED", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "VARCHAR(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Player",
                columns: table => new
                {
                    Id = table.Column<uint>(type: "INT UNSIGNED", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "VARCHAR(20)", maxLength: 20, nullable: false),
                    Email = table.Column<string>(type: "VARCHAR(100)", maxLength: 100, nullable: false),
                    Description = table.Column<string>(type: "VARCHAR(100)", maxLength: 100, nullable: false),
                    PasswordHash = table.Column<byte[]>(type: "BLOB", nullable: false),
                    PasswordSalt = table.Column<byte[]>(type: "BLOB", nullable: false),
                    PlayerTypeId = table.Column<byte>(type: "TINYINT UNSIGNED", nullable: false),
                    PlayerGenderId = table.Column<byte>(type: "TINYINT UNSIGNED", nullable: false),
                    GamesCompleted = table.Column<uint>(type: "INT UNSIGNED", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "DATETIME(2)", nullable: false),
                    DeleteDate = table.Column<DateTime>(type: "DATETIME(2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PlayerGender_Player_PlayerGenderId",
                        column: x => x.PlayerGenderId,
                        principalTable: "PlayerGender",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PlayerType_Player_PlayerTypeId",
                        column: x => x.PlayerTypeId,
                        principalTable: "PlayerType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SpecialtyTokenGamePlayerType",
                columns: table => new
                {
                    SpecialtyTokenId = table.Column<byte>(type: "TINYINT UNSIGNED", nullable: false),
                    GamePlayerTypeId = table.Column<byte>(type: "TINYINT UNSIGNED", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => new { x.SpecialtyTokenId, x.GamePlayerTypeId });
                    table.ForeignKey(
                        name: "FK_GPT_STGPT_GPTId",
                        column: x => x.GamePlayerTypeId,
                        principalTable: "GamePlayerType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ST_STGPT_STId",
                        column: x => x.SpecialtyTokenId,
                        principalTable: "SpecialtyToken",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Map",
                columns: table => new
                {
                    Id = table.Column<uint>(type: "INT UNSIGNED", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "VARCHAR(30)", maxLength: 30, nullable: false),
                    Description = table.Column<string>(type: "VARCHAR(100)", maxLength: 100, nullable: false),
                    CreatorPlayerId = table.Column<uint>(type: "INT UNSIGNED", nullable: true),
                    CreateDate = table.Column<DateTime>(type: "DATETIME(2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Player_Map_CreatorPlayerId",
                        column: x => x.CreatorPlayerId,
                        principalTable: "Player",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Game",
                columns: table => new
                {
                    Id = table.Column<uint>(type: "INT UNSIGNED", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "VARCHAR(30)", maxLength: 30, nullable: false),
                    MapId = table.Column<uint>(type: "INT UNSIGNED", nullable: false),
                    NumberOfPlayers = table.Column<byte>(type: "TINYINT UNSIGNED", nullable: false),
                    GameStatusId = table.Column<byte>(type: "TINYINT UNSIGNED", nullable: false),
                    CreatorPlayerId = table.Column<uint>(type: "INT UNSIGNED", nullable: true),
                    CreateDate = table.Column<DateTime>(type: "DATETIME(2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Player_Game_CreatorPlayerId",
                        column: x => x.CreatorPlayerId,
                        principalTable: "Player",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GameStatus_Game_GameStatusId",
                        column: x => x.GameStatusId,
                        principalTable: "GameStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Map_Game_MapId",
                        column: x => x.MapId,
                        principalTable: "Map",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MapLocation",
                columns: table => new
                {
                    MapId = table.Column<uint>(type: "INT UNSIGNED", nullable: false),
                    LocationId = table.Column<uint>(type: "INT UNSIGNED", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => new { x.MapId, x.LocationId });
                    table.ForeignKey(
                        name: "FK_Location_MapLocation_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Location",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Map_MapLocation_MapId",
                        column: x => x.MapId,
                        principalTable: "Map",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MapTransportMode",
                columns: table => new
                {
                    MapId = table.Column<uint>(type: "INT UNSIGNED", nullable: false),
                    TransportModeId = table.Column<ushort>(type: "SMALLINT UNSIGNED", nullable: false),
                    DetectiveStartingTickets = table.Column<byte>(type: "TINYINT UNSIGNED", nullable: false),
                    FelonStartingTickets = table.Column<byte>(type: "TINYINT UNSIGNED", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => new { x.MapId, x.TransportModeId });
                    table.ForeignKey(
                        name: "FK_Map_MapTransportMode_MapId",
                        column: x => x.MapId,
                        principalTable: "Map",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TransportMode_MapTransportMode_TransportModeId",
                        column: x => x.TransportModeId,
                        principalTable: "TransportMode",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MapVertex",
                columns: table => new
                {
                    Id = table.Column<uint>(type: "INT UNSIGNED", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    MapId = table.Column<uint>(type: "INT UNSIGNED", nullable: false),
                    TransportModeId = table.Column<ushort>(type: "SMALLINT UNSIGNED", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Map_MapVertex_MapId",
                        column: x => x.MapId,
                        principalTable: "Map",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TransportMode_MapVertex_TransportModeId",
                        column: x => x.TransportModeId,
                        principalTable: "TransportMode",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "GamePlayer",
                columns: table => new
                {
                    GameId = table.Column<uint>(type: "INT UNSIGNED", nullable: false),
                    PlayerId = table.Column<uint>(type: "INT UNSIGNED", nullable: false),
                    GamePlayerTypeId = table.Column<byte>(type: "TINYINT UNSIGNED", nullable: false),
                    OrderOfPlay = table.Column<byte>(type: "TINYINT UNSIGNED", nullable: true),
                    StartingLocationId = table.Column<uint>(type: "INT UNSIGNED", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => new { x.GameId, x.PlayerId });
                    table.ForeignKey(
                        name: "FK_Game_GamePlayer_GameId",
                        column: x => x.GameId,
                        principalTable: "Game",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GamePlayerType_GamePlayer_GamePlayerTypeId",
                        column: x => x.GamePlayerTypeId,
                        principalTable: "GamePlayerType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Player_GamePlayer_PlayerId",
                        column: x => x.PlayerId,
                        principalTable: "Player",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Location_GamePlayer_StartingLocationId",
                        column: x => x.StartingLocationId,
                        principalTable: "Location",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "GamePlayerMove",
                columns: table => new
                {
                    Id = table.Column<ushort>(type: "SMALLINT UNSIGNED", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    GameId = table.Column<uint>(type: "INT UNSIGNED", nullable: false),
                    PlayerId = table.Column<uint>(type: "INT UNSIGNED", nullable: false),
                    LocationId = table.Column<uint>(type: "INT UNSIGNED", nullable: false),
                    TransportModeId = table.Column<ushort>(type: "SMALLINT UNSIGNED", nullable: false),
                    PlayerPreviousMoveId = table.Column<ushort>(type: "SMALLINT UNSIGNED", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Game_GamePlayerMove_GameId",
                        column: x => x.GameId,
                        principalTable: "Game",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Location_GamePlayerMove_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Location",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Player_GamePlayerMove_PlayerId",
                        column: x => x.PlayerId,
                        principalTable: "Player",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GamePlayerMove_GamePlayerMove_PlayerPreviousMoveId",
                        column: x => x.PlayerPreviousMoveId,
                        principalTable: "GamePlayerMove",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TransportMode_GamePlayerMove_TransportModeId",
                        column: x => x.TransportModeId,
                        principalTable: "TransportMode",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "GameSpecialtyToken",
                columns: table => new
                {
                    GameId = table.Column<uint>(type: "INT UNSIGNED", nullable: false),
                    SpecialtyTokenId = table.Column<byte>(type: "TINYINT UNSIGNED", nullable: false),
                    DetectiveStartingTokens = table.Column<byte>(type: "TINYINT UNSIGNED", nullable: false),
                    FelonStartingTokens = table.Column<byte>(type: "TINYINT UNSIGNED", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => new { x.GameId, x.SpecialtyTokenId });
                    table.ForeignKey(
                        name: "FK_Game_GameSpecialityToken_GameId",
                        column: x => x.GameId,
                        principalTable: "Game",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SpecialtyToken_GST_STI",
                        column: x => x.SpecialtyTokenId,
                        principalTable: "SpecialtyToken",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "mapvertexlocation",
                columns: table => new
                {
                    MapVertexId = table.Column<uint>(type: "INT UNSIGNED", nullable: false),
                    LocationId = table.Column<uint>(type: "INT UNSIGNED", nullable: false),
                    MapVertexLocationTypeId = table.Column<byte>(type: "TINYINT UNSIGNED", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PRIMARY", x => new { x.MapVertexId, x.LocationId, x.MapVertexLocationTypeId });
                    table.ForeignKey(
                        name: "FK_Location_MapVertexLocation_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Location",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MapVertex_MapVertexLocation_MapVertexId",
                        column: x => x.MapVertexId,
                        principalTable: "MapVertex",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MVLT_MVL_MVLTId",
                        column: x => x.MapVertexLocationTypeId,
                        principalTable: "MapVertexLocationType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "FK_Player_Game_CreatorPlayerId",
                table: "Game",
                column: "CreatorPlayerId");

            migrationBuilder.CreateIndex(
                name: "FK_GameStatus_Game_GameStatusId",
                table: "Game",
                column: "GameStatusId");

            migrationBuilder.CreateIndex(
                name: "FK_Map_Game_MapId",
                table: "Game",
                column: "MapId");

            migrationBuilder.CreateIndex(
                name: "UC_Game_Name",
                table: "Game",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "FK_GamePlayerType_GamePlayer_GamePlayerTypeId",
                table: "GamePlayer",
                column: "GamePlayerTypeId");

            migrationBuilder.CreateIndex(
                name: "FK_Player_GamePlayer_PlayerId",
                table: "GamePlayer",
                column: "PlayerId");

            migrationBuilder.CreateIndex(
                name: "FK_Location_GamePlayer_StartingLocationId",
                table: "GamePlayer",
                column: "StartingLocationId");

            migrationBuilder.CreateIndex(
                name: "FK_Game_GamePlayerMove_GameId",
                table: "GamePlayerMove",
                column: "GameId");

            migrationBuilder.CreateIndex(
                name: "FK_Location_GamePlayerMove_LocationId",
                table: "GamePlayerMove",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "FK_Player_GamePlayerMove_PlayerId",
                table: "GamePlayerMove",
                column: "PlayerId");

            migrationBuilder.CreateIndex(
                name: "FK_GamePlayerMove_GamePlayerMove_PlayerPreviousMoveId",
                table: "GamePlayerMove",
                column: "PlayerPreviousMoveId");

            migrationBuilder.CreateIndex(
                name: "FK_TransportMode_GamePlayerMove_TransportModeId",
                table: "GamePlayerMove",
                column: "TransportModeId");

            migrationBuilder.CreateIndex(
                name: "UC_GamePlayerType_Name",
                table: "GamePlayerType",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "FK_SpecialtyToken_GST_STI",
                table: "GameSpecialtyToken",
                column: "SpecialtyTokenId");

            migrationBuilder.CreateIndex(
                name: "UC_GameStatus_Name",
                table: "GameStatus",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "FK_Player_Map_CreatorPlayerId",
                table: "Map",
                column: "CreatorPlayerId");

            migrationBuilder.CreateIndex(
                name: "UC_Map_Name",
                table: "Map",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "FK_Location_MapLocation_LocationId",
                table: "MapLocation",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "FK_TransportMode_MapTransportMode_TransportModeId",
                table: "MapTransportMode",
                column: "TransportModeId");

            migrationBuilder.CreateIndex(
                name: "FK_Map_MapVertex_MapId",
                table: "MapVertex",
                column: "MapId");

            migrationBuilder.CreateIndex(
                name: "FK_TransportMode_MapVertex_TransportModeId",
                table: "MapVertex",
                column: "TransportModeId");

            migrationBuilder.CreateIndex(
                name: "FK_Location_MapVertexLocation_LocationId",
                table: "mapvertexlocation",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "FK_MVLT_MVL_MVLTId",
                table: "mapvertexlocation",
                column: "MapVertexLocationTypeId");

            migrationBuilder.CreateIndex(
                name: "UC_MapVertexLocationType_Name",
                table: "MapVertexLocationType",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "UC_Player_Email",
                table: "Player",
                column: "Email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "UC_Player_Name",
                table: "Player",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "FK_PlayerGender_Player_PlayerGenderId",
                table: "Player",
                column: "PlayerGenderId");

            migrationBuilder.CreateIndex(
                name: "FK_PlayerType_Player_PlayerTypeId",
                table: "Player",
                column: "PlayerTypeId");

            migrationBuilder.CreateIndex(
                name: "UC_PlayerGender_Name",
                table: "PlayerGender",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "UC_PlayerType_Name",
                table: "PlayerType",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "UC_SpecialtyToken_Name",
                table: "SpecialtyToken",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "FK_GPT_STGPT_GPTId",
                table: "SpecialtyTokenGamePlayerType",
                column: "GamePlayerTypeId");

            migrationBuilder.CreateIndex(
                name: "UC_TransportMode_Name",
                table: "TransportMode",
                column: "Name",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GamePlayer");

            migrationBuilder.DropTable(
                name: "GamePlayerMove");

            migrationBuilder.DropTable(
                name: "GameSpecialtyToken");

            migrationBuilder.DropTable(
                name: "MapLocation");

            migrationBuilder.DropTable(
                name: "MapTransportMode");

            migrationBuilder.DropTable(
                name: "mapvertexlocation");

            migrationBuilder.DropTable(
                name: "SpecialtyTokenGamePlayerType");

            migrationBuilder.DropTable(
                name: "Game");

            migrationBuilder.DropTable(
                name: "Location");

            migrationBuilder.DropTable(
                name: "MapVertex");

            migrationBuilder.DropTable(
                name: "MapVertexLocationType");

            migrationBuilder.DropTable(
                name: "GamePlayerType");

            migrationBuilder.DropTable(
                name: "SpecialtyToken");

            migrationBuilder.DropTable(
                name: "GameStatus");

            migrationBuilder.DropTable(
                name: "Map");

            migrationBuilder.DropTable(
                name: "TransportMode");

            migrationBuilder.DropTable(
                name: "Player");

            migrationBuilder.DropTable(
                name: "PlayerGender");

            migrationBuilder.DropTable(
                name: "PlayerType");
        }
    }
}
