﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CatchXApi.Migrations
{
    public partial class TestEnumSeeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "PlayerGender",
                columns: new[] { "Id", "Name" },
                values: new object[] { (byte)0, "Unknown" });

            migrationBuilder.InsertData(
                table: "PlayerGender",
                columns: new[] { "Id", "Name" },
                values: new object[] { (byte)1, "Male" });

            migrationBuilder.InsertData(
                table: "PlayerGender",
                columns: new[] { "Id", "Name" },
                values: new object[] { (byte)2, "Female" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "PlayerGender",
                keyColumn: "Id",
                keyValue: (byte)0);

            migrationBuilder.DeleteData(
                table: "PlayerGender",
                keyColumn: "Id",
                keyValue: (byte)1);

            migrationBuilder.DeleteData(
                table: "PlayerGender",
                keyColumn: "Id",
                keyValue: (byte)2);
        }
    }
}
