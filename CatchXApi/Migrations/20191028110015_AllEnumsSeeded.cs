﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CatchXApi.Migrations
{
    public partial class AllEnumsSeeded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "GamePlayerType",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { (byte)0, "Detective" },
                    { (byte)1, "Felon" }
                });

            migrationBuilder.InsertData(
                table: "GameStatus",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { (byte)0, "Creating" },
                    { (byte)1, "Created" },
                    { (byte)2, "InProgress" },
                    { (byte)3, "Complete" }
                });

            migrationBuilder.InsertData(
                table: "MapPathLocationType",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { (byte)0, "First" },
                    { (byte)1, "Second" }
                });

            migrationBuilder.InsertData(
                table: "PlayerType",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { (byte)0, "Human" },
                    { (byte)1, "CPU" }
                });

            migrationBuilder.InsertData(
                table: "SpecialtyToken",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { (byte)0, "BlackTicket" },
                    { (byte)1, "DoubleMove" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "GamePlayerType",
                keyColumn: "Id",
                keyValue: (byte)0);

            migrationBuilder.DeleteData(
                table: "GamePlayerType",
                keyColumn: "Id",
                keyValue: (byte)1);

            migrationBuilder.DeleteData(
                table: "GameStatus",
                keyColumn: "Id",
                keyValue: (byte)0);

            migrationBuilder.DeleteData(
                table: "GameStatus",
                keyColumn: "Id",
                keyValue: (byte)1);

            migrationBuilder.DeleteData(
                table: "GameStatus",
                keyColumn: "Id",
                keyValue: (byte)2);

            migrationBuilder.DeleteData(
                table: "GameStatus",
                keyColumn: "Id",
                keyValue: (byte)3);

            migrationBuilder.DeleteData(
                table: "MapPathLocationType",
                keyColumn: "Id",
                keyValue: (byte)0);

            migrationBuilder.DeleteData(
                table: "MapPathLocationType",
                keyColumn: "Id",
                keyValue: (byte)1);

            migrationBuilder.DeleteData(
                table: "PlayerType",
                keyColumn: "Id",
                keyValue: (byte)0);

            migrationBuilder.DeleteData(
                table: "PlayerType",
                keyColumn: "Id",
                keyValue: (byte)1);

            migrationBuilder.DeleteData(
                table: "SpecialtyToken",
                keyColumn: "Id",
                keyValue: (byte)0);

            migrationBuilder.DeleteData(
                table: "SpecialtyToken",
                keyColumn: "Id",
                keyValue: (byte)1);
        }
    }
}
