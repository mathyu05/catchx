﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CatchXApi.Migrations
{
    public partial class GamePlayerTypeEnumUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "GamePlayerType",
                keyColumn: "Id",
                keyValue: (byte)0,
                column: "Name",
                value: "Unassigned");

            migrationBuilder.UpdateData(
                table: "GamePlayerType",
                keyColumn: "Id",
                keyValue: (byte)1,
                column: "Name",
                value: "Detective");

            migrationBuilder.InsertData(
                table: "GamePlayerType",
                columns: new[] { "Id", "Name" },
                values: new object[] { (byte)2, "Felon" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "GamePlayerType",
                keyColumn: "Id",
                keyValue: (byte)2);

            migrationBuilder.UpdateData(
                table: "GamePlayerType",
                keyColumn: "Id",
                keyValue: (byte)0,
                column: "Name",
                value: "Detective");

            migrationBuilder.UpdateData(
                table: "GamePlayerType",
                keyColumn: "Id",
                keyValue: (byte)1,
                column: "Name",
                value: "Felon");
        }
    }
}
