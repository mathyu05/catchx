﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CatchXApi.Migrations
{
    public partial class GameStatusEnumUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "GameStatus",
                keyColumn: "Id",
                keyValue: (byte)3);

            migrationBuilder.UpdateData(
                table: "GameStatus",
                keyColumn: "Id",
                keyValue: (byte)2,
                column: "Name",
                value: "Complete");

            migrationBuilder.UpdateData(
                table: "GameStatus",
                keyColumn: "Id",
                keyValue: (byte)1,
                column: "Name",
                value: "InProgress");

            migrationBuilder.UpdateData(
                table: "GameStatus",
                keyColumn: "Id",
                keyValue: (byte)0,
                column: "Name",
                value: "Created");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "GameStatus",
                keyColumn: "Id",
                keyValue: (byte)0,
                column: "Name",
                value: "Creating");

            migrationBuilder.UpdateData(
                table: "GameStatus",
                keyColumn: "Id",
                keyValue: (byte)1,
                column: "Name",
                value: "Created");

            migrationBuilder.UpdateData(
                table: "GameStatus",
                keyColumn: "Id",
                keyValue: (byte)2,
                column: "Name",
                value: "InProgress");

            migrationBuilder.InsertData(
                table: "GameStatus",
                columns: new[] { "Id", "Name" },
                values: new object[] { (byte)3, "Complete" });
        }
    }
}
