﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CatchXApi.Migrations
{
    public partial class SeedTestLocations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
                @"-- https://www.latlong.net/

INSERT INTO Location(
    Name,
    Description,
    Latitude,
    Longitude
)
VALUES (
    'St John''s Wood',
    'Original Scotland Yard Map Location 1',
    51.533286,
    -0.172960
),
(
    'London Zoo',
    'Original Scotland Yard Map Location 2',
    51.536059,
    -0.154796
),
(
    'Cobden Statue',
    'Original Scotland Yard Map Location 3',
    51.534644,
    -0.138788
),
(
    'Saint Pancras'' Gardens',
    'Original Scotland Yard Map Location 4',
    51.535031,
    -0.131493
),
(
    'King''s Cross St Pancras',
    'Original Scotland Yard Map Location 13',
    51.529118,
    -0.126235
),
(
    'Euston Square Gardens',
    'Original Scotland Yard Map Location 23',
    51.527302,
    -0.130806
),
(
    'London Euston',
    'Original Scotland Yard Map Location 12',
    51.528877,
    -0.132523
),
(
    'Woodhall',
    'Original Scotland Yard Map Location 11',
    51.528520,
    -0.138633
),
(
    'Euston Tower',
    'Original Scotland Yard Map Location 22',
    51.525096,
    -0.138161
),
(
    'Chester Gate',
    'Original Scotland Yard Map Location 10',
    51.528490,
    -0.144335
),
(
    'Great Portland Street',
    'Original Scotland Yard Map Location 34',
    51.524025,
    -0.144217
),
(
    'The Susie Sainsbury Theatre',
    'Original Scotland Yard Map Location 47',
    51.523127,
    -0.151126
),
(
    'Regent''s University',
    'Original Scotland Yard Map Location 21',
    51.526178,
    -0.154061
),
(
    'Baker Street',
    'Original Scotland Yard Map Location 46',
    51.522496,
    -0.155568
),
(
    'Chalfont Court',
    'Original Scotland Yard Map Location 33',
    51.524252,
    -0.158497
),
(
    'London Central Mosque',
    'Original Scotland Yard Map Location 20',
    51.528707,
    -0.163599
),
(
    'Marylebone',
    'Original Scotland Yard Map Location 45',
    51.520947,
    -0.164226
),
(
    'Fulmer House',
    'Original Scotland Yard Map Location 32',
    51.524045,
    -0.167348
),
(
    'St Marylebone War Memorial',
    'Original Scotland Yard Map Location 9',
    51.530106,
    -0.167820
),
(
    'Edgware Road',
    'Original Scotland Yard Map Location 58',
    51.519949,
    -0.170127
),
(
    'West End Gate',
    'Original Scotland Yard Map Location 44',
    51.521849,
    -0.172691
),
(
    'Lord''s Cricket Ground',
    'Original Scotland Yard Map Location 19',
    51.527636,
    -0.174129
),
(
    'Grove Court',
    'Original Scotland Yard Map Location 8',
    51.530599,
    -0.176597
),
(
    'Paddington',
    'Original Scotland Yard Map Location 74',
    51.514471,
    -0.177605
),
(
    'Winterton House',
    'Original Scotland Yard Map Location 31',
    51.525927,
    -0.178388
),
(
    'Little Venice',
    'Original Scotland Yard Map Location 57',
    51.520987,
    -0.180513
),
(
    'Brewers Court',
    'Original Scotland Yard Map Location 73',
    51.517529,
    -0.182068
),
(
    'Vale Court',
    'Original Scotland Yard Map Location 18',
    51.528350,
    -0.181350
),
(
    'Warwick Avenue',
    'Original Scotland Yard Map Location 43',
    51.523477,
    -0.183839
)"
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM Location");
        }
    }
}
